IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BrugsArt]') AND type in (N'U'))
DROP TABLE [dbo].[BrugsArt]
GO

CREATE TABLE [dbo].[BrugsArt](
	[BrugsArtKode] [int] NOT NULL,
	[BrugsArtTekst] [nvarchar](128) NOT NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PKN_BrugsArt] PRIMARY KEY CLUSTERED 
(
	[BrugsArtKode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
