IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VeterinaerStatus]') AND type in (N'U'))
DROP TABLE [dbo].[VeterinaerStatus]
GO

CREATE TABLE [dbo].[VeterinaerStatus](
	[VeterinaerStatusKode] [nvarchar](50) NOT NULL,
	[VeterinaerStatusTekst] [nvarchar](128) NOT NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PKN_VeterinaerStatus] PRIMARY KEY CLUSTERED 
(
	[VeterinaerStatusKode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
