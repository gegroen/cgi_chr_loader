﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CGI_CHR_loader
{
    public class InfoFrame : IDisposable
    {
        private string _spinBar = "■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■ ■";
        private string _dotBar = ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ";
        private bool _active;
        private readonly Thread _thread;
        private readonly int _delay;

        private int _frameWidth = 78;
        private int _blockCount = 59;
        private double _currentProgress;
        private char[] _chars = new[] { ' ', '▄', '█', '▀' };
        private int _charindex = 0;
        private string _latestError = null;
        private int _curx = 0;
        private int _curw = 46;

        public int Ejendomme { get; set; }
        public int Besætninger { get; set; }
        public int Ejere { get; set; }
        public int Brugere { get; set; }
        public int Praksis { get; set; }
        public int BesætningsTyper { get; set; }
        public int Dyreart { get; set; }
        public int Brugasrter { get; set; }
        public int Omsætning { get; set; }
        public int Sygdom { get; set; }
        public int VeterinaerStatus { get; set; }
        public int SygdomsNiveau { get; set; }
        public int VeterinaereHaendelser { get; set; }
        public int VeterinaereHaendelse { get; set; }
        public int EjendomListBesaetning { get; set; }
        public int EjendomListBruger { get; set; }
        public int EjendomListDyrlaege { get; set; }
        public int EjendomListEjer { get; set; }
        public int Kommuner { get; set; }


        public int Retry { get; set; }

        public Stopwatch Stopwatch1 { get; set; }
        public Stopwatch Stopwatch2 { get; set; }

        public bool Spinner { get; set; }
        public ConsoleColor BarColor { get; set; } = ConsoleColor.Magenta;
        public double Max { get; set; }
        public double Value { get; set; }

        public string LatestError
        {
            set
            {
                _latestError = new string(' ', _curw) + value + new string(' ', _curw);
            }
        }

        public bool Pause
        {
            get { return _active; }
            set
            {
                _active = value;
            }
        }
        public string Header { get; set; } = "";
        public string Info { get; set; } = "";


        public InfoFrame()
        {
            Stopwatch1 = new Stopwatch();
            Stopwatch2 = new Stopwatch();
            _delay = 250;

            Ejendomme = 0;
            Besætninger = 0;
            Ejere = 0;
            Brugere = 0;
            Praksis = 0;
            BesætningsTyper = 0;
            Dyreart = 0;
            Brugasrter = 0;
            Retry = 0;

            _thread = new Thread(Tick);
        }

        private void Tick()
        {
            while (_active)
            {
                Draw();
                Thread.Sleep(_delay);
            }
        }

        public void Start()
        {
            Console.CursorVisible = false;
            Stopwatch1.Start();

            Console.Clear();
            DrawFrame();

            if (!_thread.IsAlive)
            {
                _active = true;
                _thread.Start();
            }
        }

        public void Stop()
        {
            Stopwatch1.Stop();
            Draw();
            _active = false;
            Console.SetCursorPosition(50, 13);
            Console.WriteLine();
            Console.CursorVisible = true;
        }

        public void Report()
        {
            var value = Math.Max(0, Math.Min(1, Value <= 0 ? 1 : Value / Max));
            Interlocked.Exchange(ref _currentProgress, value);
        }
        
        public void Dispose()
        {
            Stop();
        }

        private string oldheader;
        private TimeSpan oldElapsedTime;
        private int oldRetry;
        private TimeSpan oldRetryElapsed;
        private int oldEjendomme;
        private int oldBesætninger;
        private int oldEjere;
        private int oldBrugere;
        private int oldPraksis;
        private int oldBesætningsTyper;
        private int oldDyreart;
        private int oldBrugasrter;
        private int oldOmsætning;
        private int oldSygdom;
        private int oldVeterinaerStatus;
        private int oldSygdomsNiveau;
        private int oldVeterinaereHaendelser;
        private int oldVeterinaereHaendelse;
        private int oldEjendomListBesaetning;
        private int oldEjendomListBruger;
        private int oldEjendomListDyrlaege;
        private int oldEjendomListEjer;
        private int oldKommuner;

        private void Draw()
        {
            if (!_active) return;
            var oldfgc = Console.ForegroundColor;

            var y = 1;
            // Header
            if (oldheader != Header)
            {
                oldheader = Header;
                var x = ((Header.Length == 0) ? 0 : ((_frameWidth - Header.Length) / 2));
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.SetCursorPosition(1, y);
                Console.Write(new string(' ', _frameWidth - 2));
                if (Header.Length > 0)
                {
                    Console.SetCursorPosition(x, y);
                    Console.Write($"{Header}");
                }
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;
            }

            y = 3;
            // Stopwaytch 1
            if (oldElapsedTime != Stopwatch1.Elapsed)
            {
                oldElapsedTime = Stopwatch1.Elapsed;
                var str = $"{oldElapsedTime.Days:0}.{oldElapsedTime.Hours:00}:{oldElapsedTime.Minutes:00}:{oldElapsedTime.Seconds:00}.{oldElapsedTime.Milliseconds:000}";
                Console.SetCursorPosition(14, y);
                Console.Write(str);
            }
            // Retry
            if (oldRetry != Retry)
            {
                oldRetry = Retry;
                DrawNumber(9, y, $"{Retry:##,###}", 10);
                Console.SetCursorPosition(38, y);
            }
            // Retry Timer
            if(oldRetryElapsed != Stopwatch2.Elapsed)
            {
                oldRetryElapsed = Stopwatch2.Elapsed;
                var str = $"{oldRetryElapsed.Days:0}.{oldRetryElapsed.Hours:00}:{oldRetryElapsed.Minutes:00}:{oldRetryElapsed.Seconds:00}.{oldRetryElapsed.Milliseconds:000}";
                Console.SetCursorPosition(60, y);
                Console.Write(str);
            }
            
            y = 5; // 15, 41, 67
            // Ejendomme
            if (oldEjendomme != Ejendomme)
            {
                oldEjendomme = Ejendomme;
                DrawNumber(15, y, $"{Ejendomme:##,###}", 10);
            }
            // Besætninger
            if (oldBesætninger != Besætninger)
            {
                oldBesætninger = Besætninger;
                DrawNumber(41, y, $"{Besætninger:##,###}", 10);
            }
            // Ejer
            if (oldEjere != Ejere)
            {
                oldEjere = Ejere;
                DrawNumber(67, y, $"{Ejere:##,###}", 10);
            }

            y = 6;
            // Brugere
            if (oldBrugere != Brugere)
            {
                oldBrugere = Brugere;
                DrawNumber(15, y, $"{Brugere:##,###}", 10);
            }
            // Praksis
            if (oldPraksis != Praksis)
            {
                oldPraksis = Praksis;
                DrawNumber(41, y, $"{Praksis:##,###}", 10);
            }
            // BesætningsTyper
            if (oldBesætningsTyper != BesætningsTyper)
            {
                oldBesætningsTyper = BesætningsTyper;
                DrawNumber(67, y, $"{BesætningsTyper:##,###}", 10);
            }

            y = 7;
            // Dyreart
            if (oldDyreart != Dyreart)
            {
                oldDyreart = Dyreart;
                DrawNumber(15, y, $"{Dyreart:##,###}", 10);
            }
            // Brugasrter
            if (oldBrugasrter != Brugasrter)
            {
                oldBrugasrter = Brugasrter;
                DrawNumber(41, y, $"{Brugasrter:##,###}", 10);
            }
            // Omsætning
            if(oldOmsætning != Omsætning)
            {
                oldOmsætning = Omsætning;
                DrawNumber(67, y, $"{Omsætning:##,###}", 10);
            }

            y = 8;
            // Sygdom
            if(oldSygdom != Sygdom)
            {
                oldSygdom = Sygdom;
                DrawNumber(15, y, $"{Sygdom:##,###}", 10);
            }
            // VeterinaerStatus
            if(oldVeterinaerStatus != VeterinaerStatus)
            {
                oldVeterinaerStatus = VeterinaerStatus;
                DrawNumber(41, y, $"{VeterinaerStatus:##,###}", 10);
            }
            // SygdomsNiveau
            if(oldSygdomsNiveau != SygdomsNiveau)
            {
                oldSygdomsNiveau = SygdomsNiveau;
                DrawNumber(67, y, $"{SygdomsNiveau:##,###}", 10);
            }

            y = 9;
            // VeterinaereHaendelser
            if (oldVeterinaereHaendelser != VeterinaereHaendelser)
            {
                oldVeterinaereHaendelser = VeterinaereHaendelser;
                DrawNumber(15, y, $"{VeterinaereHaendelser:##,###}", 10);
            }
            // VeterinaereHaendelse
            if (oldVeterinaereHaendelse != VeterinaereHaendelse)
            {
                oldVeterinaereHaendelse = VeterinaereHaendelse;
                DrawNumber(41, y, $"{VeterinaereHaendelse:##,###}", 10);
            }
            // EjendomListBesaetning
            if (oldEjendomListBesaetning != EjendomListBesaetning)
            {
                oldEjendomListBesaetning = EjendomListBesaetning;
                DrawNumber(67, y, $"{EjendomListBesaetning:##,###}", 10);
            }

            y = 10;
            // EjendomListBruger
            if (oldEjendomListBruger != EjendomListBruger)
            {
                oldEjendomListBruger = EjendomListBruger;
                DrawNumber(15, y, $"{EjendomListBruger:##,###}", 10);
            }
            // EjendomListDyrlaege
            if (oldEjendomListDyrlaege != EjendomListDyrlaege)
            {
                oldEjendomListDyrlaege = EjendomListDyrlaege;
                DrawNumber(41, y, $"{EjendomListDyrlaege:##,###}", 10);
            }
            // EjendomListEjer
            if (oldEjendomListEjer != EjendomListEjer)
            {
                oldEjendomListEjer = EjendomListEjer;
                DrawNumber(67, y, $"{EjendomListEjer:##,###}", 10);
            }
            
            y = 11;
            // Kommuner
            if (oldKommuner != Kommuner)
            {
                oldKommuner = Kommuner;
                DrawNumber(15, y, $"{Kommuner:##,###}", 10);
            }

            y = 13;
            if (Spinner)
            {
                var text = _spinBar.Substring(_charindex, _blockCount);
                _charindex = _charindex == 0 ? 1 : 0;
                Console.SetCursorPosition(13, y);
                Console.ForegroundColor = BarColor;
                Console.Write(text);
                Console.ForegroundColor = oldfgc;
                Console.SetCursorPosition(13 + _blockCount + 1, y++);
                Console.Write($"    ");
                Console.SetCursorPosition(13, y);
                Console.Write(new string(' ', _blockCount));
            }
            else
            {
                var progressBlockCount = (int)(_currentProgress * _blockCount);
                var percent = (int)(_currentProgress * 100);

                var text = $"{new string('■', progressBlockCount)}{_dotBar.Substring(_charindex, _blockCount - progressBlockCount)}";
                _charindex = _charindex == 0 ? 1 : 0;

                Console.SetCursorPosition(13, y);
                Console.ForegroundColor = BarColor;
                Console.Write(text);
                Console.ForegroundColor = oldfgc;
                Console.SetCursorPosition(13 + _blockCount + 1, y++);
                Console.Write($"{percent,3}%");

                var str = $"{Value:##,###} of {Max:##,###}";
                var xx = (_blockCount - str.Length) / 2;
                Console.SetCursorPosition(12, y);
                Console.Write(new string(' ', _blockCount));
                Console.SetCursorPosition(12 + xx, y);
                Console.Write(str);
            }
            y++;


            // Draw Latest Error Message
            //y = 7;
            //if (!string.IsNullOrEmpty(_latestError))
            //{
            //    Console.ForegroundColor = ConsoleColor.Red;
            //    Console.SetCursorPosition(2, y);
            //    Console.Write(_latestError.Substring(_curx++, _curw));
            //    Console.ForegroundColor = ConsoleColor.White;

            //    if (_curx + _curw >= _latestError.Length)
            //        _curx = 0;
            //}
            
            Console.SetCursorPosition(0, y);
        }

        private void DrawNumber(int x, int y, string value, int len)
        {
            string spacer = new string(' ', len);
            Console.SetCursorPosition(x, y);
            Console.Write(spacer.Substring(0, len - value.Length) + value);
        }

        private static void DrawFrame()
        {
            Console.ForegroundColor = ConsoleColor.White;
            // ┌─┐└┘├┤┬┴┼│
            //             01234567890123456789012345678901234567890123456789
            //WriteEscCodes("┌────────────────────────────────────────────────┐");
            //WriteEscCodes("│$B{Gray}                                                $B{Black}│");
            //WriteEscCodes("├───────────────────┬────────────────────────────┤");
            //WriteEscCodes("│                   │ $F{Yellow}Elapsed:                   $F{White}│");
            //WriteEscCodes("├───────────────────┼────────────────────────────┤");
            //WriteEscCodes("│ $F{Yellow}Retry:            $F{White}│ $F{Yellow}Elapsed:                   $F{White}│");
            //WriteEscCodes("├───────────────────┴────────────────────────────┤");
            //WriteEscCodes("│                                                │");
            //WriteEscCodes("├───────────────────────┬────────────────────────┤");
            //WriteEscCodes("│ $F{Yellow}Ejendomme:            $F{White}│ $F{Yellow}Besætninger:           $F{White}│");
            //WriteEscCodes("│ $F{Yellow}Ejere:                $F{White}│ $F{Yellow}Brugere:               $F{White}│");
            //WriteEscCodes("│ $F{Yellow}Praksis:              $F{White}│ $F{Yellow}Besæt. Typer:          $F{White}│");
            //WriteEscCodes("│ $F{Yellow}Dyreart:              $F{White}│ $F{Yellow}Brugasrter:            $F{White}│");
            //WriteEscCodes("├───────────────────────┴────────────────────────┤");
            //WriteEscCodes("│ $F{Yellow}Progress: $F{White}[                              ]     │");
            //WriteEscCodes("│                                                │");
            //WriteEscCodes("└────────────────────────────────────────────────┘");

            //             0         0         0         0         0         0         0         0         0
            WriteEscCodes("┌─────────────────────────────────────────────────────────────────────────────┐");
            WriteEscCodes("│$B{Gray}                                                                             $B{Black}│");
            WriteEscCodes("├────────────────────────────┬───────────────────┬────────────────────────────┤");
            WriteEscCodes("│ $F{Yellow}Elapsed:                   $F{White}│ $F{Yellow}Retry:            $F{White}│ $F{Yellow}Elapsed:                   $F{White}│");
            WriteEscCodes("├─────────────────────────┬──┴───────────────────┴──┬─────────────────────────┤");
            WriteEscCodes("│ $F{Yellow}Ejendom:                $F{White}│ $F{Yellow}Besætning:              $F{White}│ $F{Yellow}Ejer:                   $F{White}│");
            WriteEscCodes("│ $F{Yellow}Bruger:                 $F{White}│ $F{Yellow}Praksis:                $F{White}│ $F{Yellow}Besæt.Type:             $F{White}│");
            WriteEscCodes("│ $F{Yellow}DyreArt:                $F{White}│ $F{Yellow}BrugsArt:               $F{White}│ $F{Yellow}Omsaetning:             $F{White}│");
            WriteEscCodes("│ $F{Yellow}Sygdom:                 $F{White}│ $F{Yellow}Vetr.Status:            $F{White}│ $F{Yellow}Sygd.Niveau:            $F{White}│");
            WriteEscCodes("│ $F{Yellow}Vetr.Hænde.:            $F{White}│ $F{Yellow}Vetr.H.:                $F{White}│ $F{Yellow}EjndListBes:            $F{White}│");
            WriteEscCodes("│ $F{Yellow}EjndListBru:            $F{White}│ $F{Yellow}EjndListDyr:            $F{White}│ $F{Yellow}EjndListEjer:           $F{White}│");
            WriteEscCodes("│ $F{Yellow}Kommuner:               $F{White}│                         │                         │");
            WriteEscCodes("├─────────────────────────┴─────────────────────────┴─────────────────────────┤");
            WriteEscCodes("│ $F{Yellow}Progress: $F{White}[                                                           ]     │");
            WriteEscCodes("│                                                                             │");
            WriteEscCodes("├─────────────────────────────────────────────────────────────────────────────┤");
            WriteEscCodes("│                                                                             │");
            WriteEscCodes("└─────────────────────────────────────────────────────────────────────────────┘");

        }



        private static void WriteEscCodes(string str)
        {
            for (var i = 0; i < str.Length; i++)
            {
                var cmd = i + 3 < str.Length ? str.Substring(i, 3).ToUpper() : "";

                if (cmd == "$F{" || cmd == "$B{")
                {
                    i += 3;
                    var st = i;
                    var cutlen = 0;
                    while (i < str.Length && str.Substring(i, 1) != "}")
                    {
                        i++;
                        cutlen++;
                    }

                    if (cutlen > 0)
                    {
                        if (cmd == "$F{")
                            Console.ForegroundColor = GetStringColor(str.Substring(st, cutlen));
                        else
                            Console.BackgroundColor = GetStringColor(str.Substring(st, cutlen));
                    }
                    //i = st + cutlen;
                }
                else Console.Write(str.Substring(i, 1));

            }
            Console.WriteLine();
        }

        private static ConsoleColor GetStringColor(string str)
        {
            var cRet = Console.ForegroundColor;

            switch (str)
            {
                case "Black": cRet = ConsoleColor.Black; break;
                case "DarkBlue": cRet = ConsoleColor.DarkBlue; break;
                case "DarkGreen": cRet = ConsoleColor.DarkGreen; break;
                case "DarkCyan": cRet = ConsoleColor.DarkCyan; break;
                case "DarkRed": cRet = ConsoleColor.DarkRed; break;
                case "DarkMagenta": cRet = ConsoleColor.DarkMagenta; break;
                case "DarkYellow": cRet = ConsoleColor.DarkYellow; break;
                case "Gray": cRet = ConsoleColor.Gray; break;
                case "DarkGray": cRet = ConsoleColor.DarkGray; break;
                case "Blue": cRet = ConsoleColor.Blue; break;
                case "Green": cRet = ConsoleColor.Green; break;
                case "Cyan": cRet = ConsoleColor.Cyan; break;
                case "Red": cRet = ConsoleColor.Red; break;
                case "Magenta": cRet = ConsoleColor.Magenta; break;
                case "Yellow": cRet = ConsoleColor.Yellow; break;
                case "White": cRet = ConsoleColor.White; break;
            }

            return cRet;
        }

    }
}
