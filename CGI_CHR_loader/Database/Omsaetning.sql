IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Omsaetning]') AND type in (N'U'))
DROP TABLE [dbo].[Omsaetning]
GO

CREATE TABLE [dbo].[Omsaetning](
	[OmsaetningsKode] [int] NOT NULL,
	[OmsaetningsTekst] [nvarchar](128) NOT NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PKN_Omsaetning] PRIMARY KEY CLUSTERED 
(
	[OmsaetningsKode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
