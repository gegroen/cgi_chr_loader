﻿using CGI_CHR_loader.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader
{
    class MatchOwnerAgainstRagtime
    {
        private static char[] _trimchars = { ' ', ',', '.', '\t' };

        /// <summary>
        /// Matches the specified ejere.
        /// </summary>
        /// <param name="ejere">The ejere.</param>
        /// <param name="info">The information.</param>
        /// <returns></returns>
        /// This element was inserted by gergro on machine BBIPC0551.18-04-2018.12:03
        public static IEnumerable<EjerListRagtimeStamdata> Match(IEnumerable<Ejer> ejere, InfoFrame info)
        {
            var ejerlistragtimestamdata = new List<EjerListRagtimeStamdata>();

            var sqlconnectionstring = ConfigurationManager.ConnectionStrings["RAGTIME_CONN"].ToString();
            using (var conn = new SqlConnection(sqlconnectionstring))
            {
                conn.Open();

                info.Max = ejere.Count();
                info.Value = 0;
                info.Spinner = false;

                foreach (var ejer in ejere)
                {
                    info.Value++;
                    info.Report();

                    if (ejer.CVR != 0)
                    {
                        int gennemloeb = 0;
                        decimal TDC_ID = 0;
                        var stamdata = GetRagtimeStamdatas(conn, ejer.CVR).ToList();

                        if (stamdata.Any())
                        {
                            if (stamdata.Count == 1)
                            {
                                gennemloeb = MatchGennemloeb(ejer, stamdata[0]);
                                if (gennemloeb > 0) TDC_ID = stamdata[0].TDC_ID;
                            }
                            else
                            {
                                int minGennemloeb = 100;
                                foreach (var item in stamdata)
                                {
                                    //log.Append(string.Format("  item.Navn: {0}", item.Navn), Log.LogLevels.Debug);
                                    int tmpgennemloeb = MatchGennemloeb(ejer, item);
                                    if (tmpgennemloeb > 0 && tmpgennemloeb <= minGennemloeb)
                                    {
                                        minGennemloeb = tmpgennemloeb;
                                        gennemloeb = minGennemloeb;
                                        TDC_ID = item.TDC_ID;
                                    }
                                }
                            }
                        }


                        if (gennemloeb == 0)
                        {
                            gennemloeb = MatchWhitoutCvrNumber(ejer, conn, out TDC_ID);
                            if (gennemloeb == 0) TDC_ID = 0;
                        }

                        if (TDC_ID != 0 && gennemloeb > 0)
                        {
                            ejerlistragtimestamdata.Add(new EjerListRagtimeStamdata
                            {
                                TDC_ID = TDC_ID,
                                EjerID = ejer.EjerID,
                                Status = gennemloeb
                            });
                        }

                    }
                }
            }
            return ejerlistragtimestamdata;
        }

        /// <summary>
        /// Gets the ragtime stamdatas.
        /// </summary>
        /// <param name="conn">The connection.</param>
        /// <param name="cvr">The CVR.</param>
        /// <returns></returns>
        /// This element was inserted by gergro on machine BBIPC0551.18-04-2018.10:48
        private static IEnumerable<RagtimeStamdata> GetRagtimeStamdatas(SqlConnection conn, int cvr)
        {
            if(conn?.State == ConnectionState.Closed)
                conn.Open();
            
            var stamdata = new List<RagtimeStamdata>();
            using (SqlCommand cmd = new SqlCommand($"SELECT * FROM [Ragtime].[dbo].[stamdata] WHERE CVRnr={cvr}", conn))
            {
                cmd.CommandTimeout = (60 * 60) * 8;
                using (SqlDataReader rd = cmd.ExecuteReader())
                {
                    var oTDC_ID = rd.GetOrdinal("TDC_ID");
                    var oNavn = rd.GetOrdinal("Juridisk_Navn");
                    var oAdresse = rd.GetOrdinal("Adresse");
                    var oPostnr = rd.GetOrdinal("Postnr");
                    var oBynavn = rd.GetOrdinal("Bynavn");
                    var oCVRnr = rd.GetOrdinal("CVRnr");
                    var oHovedAfd = rd.GetOrdinal("HovedAfd");
                    var oFirma_status = rd.GetOrdinal("Firma_status");

                    while (rd.Read())
                    {
                        string tmp = (rd.IsDBNull(oFirma_status)) ? "" : rd.GetString(oFirma_status).Trim(_trimchars);
                        Int32.TryParse(tmp, out var Firma_status);

                        stamdata.Add(new RagtimeStamdata
                        {
                            TDC_ID = rd.IsDBNull(oTDC_ID) ? 0 : rd.GetDecimal(oTDC_ID),
                            Navn = rd.IsDBNull(oNavn) ? "" : rd.GetString(oNavn),
                            Adresse = rd.IsDBNull(oAdresse) ? "" : rd.GetString(oAdresse),
                            Postnr = rd.IsDBNull(oPostnr) ? 0 : rd.GetInt16(oPostnr),
                            Bynavn = rd.IsDBNull(oBynavn) ? "" : rd.GetString(oBynavn),
                            CVRnr = rd.IsDBNull(oCVRnr) ? 0 : rd.GetInt32(oCVRnr),
                            HovedAfd = rd.IsDBNull(oHovedAfd) ? 0 : rd.GetInt32(oHovedAfd),
                            Firma_status = Firma_status,
                        });
                    }
                }
            }

            return stamdata;
        }

        /// <summary>
        /// Matches the gennemloeb.
        /// </summary>
        /// <param name="ejer">The ejer.</param>
        /// <param name="stamdata">The stamdata.</param>
        /// <returns></returns>
        /// This element was inserted by gergro on machine BBIPC0551.18-04-2018.10:58
        private static int MatchGennemloeb(Ejer ejer, RagtimeStamdata stamdata)
        {
            int gennemloeb = 0;

            bool matchNavn = (Phonetic.Match(ejer.Navn, stamdata.Navn));
            bool matchAdresse = (Phonetic.Match(ejer.Adresse, stamdata.Adresse));
            bool matchAktiv = (stamdata.Firma_status == 0);
            bool matchHovedAfd = (stamdata.HovedAfd == 1);
            bool matchPostnr = (ejer.PostNummer == stamdata.Postnr);
            bool matchBynavn = (Phonetic.Match(ejer.ByNavn, stamdata.Bynavn));

            // Gennemløb 1
            if (matchNavn && matchAdresse && matchAktiv && matchHovedAfd && (matchPostnr || matchBynavn))
                gennemloeb = 1;
            else if (matchNavn && matchAktiv && matchHovedAfd)
                gennemloeb = 2;
            else if (matchAdresse && matchAktiv && matchHovedAfd && (matchPostnr || matchBynavn))
                gennemloeb = 3;
            else if (matchNavn && matchAdresse && matchAktiv && (matchPostnr || matchBynavn))
                gennemloeb = 4;
            else if (matchNavn && matchAdresse && matchHovedAfd && (matchPostnr || matchBynavn))
                gennemloeb = 5;
            else if (matchNavn && matchAdresse && (matchPostnr || matchBynavn))
                gennemloeb = 6;

            if (gennemloeb == 0)
            {
                matchNavn = (Phonetic.Match(ejer.Navn, stamdata.Navn, 100));
                matchAdresse = (Phonetic.Match(ejer.Adresse, stamdata.Adresse, 100));
                matchBynavn = (Phonetic.Match(ejer.ByNavn, stamdata.Bynavn, 100));

                if (matchNavn && matchAdresse && matchAktiv && matchHovedAfd && (matchPostnr || matchBynavn))
                    gennemloeb = 7;
                else if (matchNavn && matchAktiv && matchHovedAfd)
                    gennemloeb = 8;
                else if (matchAdresse && matchAktiv && matchHovedAfd && (matchPostnr || matchBynavn))
                    gennemloeb = 9;
                else if (matchNavn && matchAdresse && matchAktiv && (matchPostnr || matchBynavn))
                    gennemloeb = 10;
                else if (matchNavn && matchAdresse && matchHovedAfd && (matchPostnr || matchBynavn))
                    gennemloeb = 11;
                else if (matchNavn && matchAdresse && (matchPostnr || matchBynavn))
                    gennemloeb = 12;
            }
            if (gennemloeb == 0)
            {
                matchNavn = (Phonetic.Match(ejer.Navn, stamdata.Navn, 80));
                matchAdresse = (Phonetic.Match(ejer.Adresse, stamdata.Adresse, 80));
                matchBynavn = (Phonetic.Match(ejer.ByNavn, stamdata.Bynavn, 80));

                if (matchNavn && matchAdresse && matchAktiv && matchHovedAfd && (matchPostnr || matchBynavn))
                    gennemloeb = 13;
                else if (matchNavn && matchAktiv && matchHovedAfd)
                    gennemloeb = 14;
                else if (matchAdresse && matchAktiv && matchHovedAfd && (matchPostnr || matchBynavn))
                    gennemloeb = 15;
                else if (matchNavn && matchAdresse && matchAktiv && (matchPostnr || matchBynavn))
                    gennemloeb = 16;
                else if (matchNavn && matchAdresse && matchHovedAfd && (matchPostnr || matchBynavn))
                    gennemloeb = 17;
                else if (matchNavn && matchAdresse && (matchPostnr || matchBynavn))
                    gennemloeb = 18;
            }

            return gennemloeb;
        }

        /// <summary>
        /// Matches the whitout CVR number.
        /// </summary>
        /// <param name="ejer">The ejer.</param>
        /// <param name="tdc_id">The TDC identifier.</param>
        /// <returns></returns>
        /// This element was inserted by gergro on machine BBIPC0551.18-04-2018.10:58
        private static int MatchWhitoutCvrNumber(Ejer ejer, SqlConnection con, out decimal tdc_id)
        {
            tdc_id = 0;
            int gennemloeb = 0;

            string sql = "SELECT * FROM [Ragtime].[dbo].[stamdata] WHERE HovedAfd=1 AND Firma_status='0'";
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                cmd.CommandTimeout = (60 * 60) * 8;
                using (SqlDataReader rd = cmd.ExecuteReader())
                {
                    int tdcidID = rd.GetOrdinal("TDC_ID");
                    int navnID = rd.GetOrdinal("Juridisk_Navn");
                    int adresseID = rd.GetOrdinal("Adresse");
                    int postnrID = rd.GetOrdinal("Postnr");
                    int BynavnID = rd.GetOrdinal("Bynavn");
                    int cvrbrID = rd.GetOrdinal("CVRnr");
                    int hovedafdID = rd.GetOrdinal("HovedAfd");
                    int tmpID = rd.GetOrdinal("Firma_status");

                    while (rd.Read())
                    {
                        decimal tdcid = (rd.IsDBNull(tdcidID)) ? 0 : rd.GetDecimal(tdcidID);
                        string navn = (rd.IsDBNull(navnID)) ? "" : rd.GetString(navnID).Trim(_trimchars);
                        string adresse = (rd.IsDBNull(adresseID)) ? "" : rd.GetString(adresseID).Trim(_trimchars);
                        int postnr = (rd.IsDBNull(postnrID)) ? 0 : rd.GetInt16(postnrID);
                        string bynavn = (rd.IsDBNull(BynavnID)) ? "" : rd.GetString(BynavnID).Trim(_trimchars);
                        int cvrbr = (rd.IsDBNull(cvrbrID)) ? 0 : rd.GetInt32(cvrbrID);
                        int hovedafd = (rd.IsDBNull(hovedafdID)) ? 0 : rd.GetInt32(hovedafdID);
                        string tmp = (rd.IsDBNull(tmpID)) ? "" : rd.GetString(tmpID).Trim(_trimchars);
                        int firmastatus = 0;
                        if (!string.IsNullOrEmpty(tmp)) Int32.TryParse(tmp, out firmastatus);
                        var stamdata = new RagtimeStamdata
                        {
                            TDC_ID = tdcid,
                            Navn = navn,
                            Adresse = adresse,
                            Postnr = postnr,
                            Bynavn = bynavn,
                            CVRnr = cvrbr,
                            HovedAfd = hovedafd,
                            Firma_status = firmastatus
                        };

                        bool matchNavn = (Phonetic.Match(ejer.Navn, stamdata.Navn));
                        bool matchAdresse = (Phonetic.Match(ejer.Adresse, stamdata.Adresse));
                        bool matchPostnr = (ejer.PostNummer == stamdata.Postnr);
                        bool matchBynavn = (Phonetic.Match(ejer.ByNavn, stamdata.Bynavn));

                        if (matchNavn && matchAdresse && (matchPostnr || matchBynavn))
                        {
                            tdc_id = tdcid;
                            gennemloeb = 19;
                        }
                        else
                        {
                            matchNavn = (Phonetic.Match(ejer.Navn, stamdata.Navn, 100));
                            matchAdresse = (Phonetic.Match(ejer.Adresse, stamdata.Adresse, 100));
                            matchPostnr = (ejer.PostNummer == stamdata.Postnr);
                            matchBynavn = (Phonetic.Match(ejer.ByNavn, stamdata.Bynavn, 100));
                            if (matchNavn && matchAdresse && (matchPostnr || matchBynavn))
                            {
                                tdc_id = tdcid;
                                gennemloeb = 20;
                            }
                            else
                            {
                                matchNavn = (Phonetic.Match(ejer.Navn, stamdata.Navn, 80));
                                matchAdresse = (Phonetic.Match(ejer.Adresse, stamdata.Adresse, 80));
                                matchPostnr = (ejer.PostNummer == stamdata.Postnr);
                                matchBynavn = (Phonetic.Match(ejer.ByNavn, stamdata.Bynavn, 80));
                                if (matchNavn && matchAdresse && (matchPostnr || matchBynavn))
                                {
                                    tdc_id = tdcid;
                                    gennemloeb = 21;
                                }
                            }
                        }
                        if (gennemloeb != 0)
                            break;
                    }

                }
            }
            con.Close();
            return gennemloeb;
        }

    }
}
