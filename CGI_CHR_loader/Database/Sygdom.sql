IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sygdom]') AND type in (N'U'))
DROP TABLE [dbo].[Sygdom]
GO

CREATE TABLE [dbo].[Sygdom](
	[SygdomsKode] [nvarchar](50) NOT NULL,
	[SygdomsTekst] [nvarchar](128) NOT NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PKN_Sygdom] PRIMARY KEY CLUSTERED 
(
	[SygdomsKode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
