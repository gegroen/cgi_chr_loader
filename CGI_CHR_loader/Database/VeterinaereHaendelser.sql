IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VeterinaereHaendelser]') AND type in (N'U'))
DROP TABLE [dbo].[VeterinaereHaendelser]
GO

CREATE TABLE [dbo].[VeterinaereHaendelser](
	[CHR] [int] NOT NULL,
	[VeterinaereProblemer] [bit] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PKN_VeterinaereHaendelser] PRIMARY KEY CLUSTERED 
(
	[CHR] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
