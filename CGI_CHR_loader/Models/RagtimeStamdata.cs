﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    class RagtimeStamdata
    {
        public decimal TDC_ID { get; set; }
        public string Navn { get; set; }
        public string Adresse { get; set; }
        public int Postnr { get; set; }
        public string Bynavn { get; set; }
        public int CVRnr { get; set; }
        public int HovedAfd { get; set; }
        public int Firma_status { get; set; }
    }
}
