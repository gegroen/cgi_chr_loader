﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    public class EjerListRagtimeStamdata
    {
        public decimal TDC_ID { get; set; }
        public int EjerID { get; set; }
        public int Status { get; set; }
    }
}
