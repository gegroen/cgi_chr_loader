﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    public class VeterinaereHaendelse
    {
        public int ID { get; set; }
        public int CHR { get; set; }
        public int DyreArtKode { get; set; }
        public string SygdomsKode { get; set; }
        public string VeterinaerStatusKode { get; set; }
        public int SygdomsNiveauKode { get; set; }
        public DateTime DatoVeterinaerStatus { get; set; }
    }
}
