IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kommune]') AND type in (N'U'))
DROP TABLE [dbo].[Kommune]
GO

CREATE TABLE [dbo].[Kommune](
	[KommuneID] [int] NULL,
	[Navn] [nvarchar](128) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
) ON [PRIMARY]
GO
