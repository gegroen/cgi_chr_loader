IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DyreArt]') AND type in (N'U'))
DROP TABLE [dbo].[DyreArt]
GO

CREATE TABLE [dbo].[DyreArt](
	[DyreArtKode] [int] NOT NULL,
	[DyreArtTekst] [nvarchar](128) NOT NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PKN_DyreArt] PRIMARY KEY CLUSTERED 
(
	[DyreArtKode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
