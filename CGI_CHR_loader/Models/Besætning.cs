﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    public class Besætning
    {
        public int BeseatningsID { get; set; }
        public long besaetningsNummer { get; set; }
        public int dyreArtKode { get; set; }
        public string dyreArtTekst { get; set; }
        public int brugsArtKode { get; set; }
        public string brugsArtTekst { get; set; }
        public string virksomhedsArtTekst { get; set; }
        public int omsaetningsKode { get; set; }
        public string omsaetningsTekst { get; set; }
    }
}
