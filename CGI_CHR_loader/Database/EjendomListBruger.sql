IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EjendomListBruger]') AND type in (N'U'))
DROP TABLE [dbo].[EjendomListBruger]
GO

CREATE TABLE [dbo].[EjendomListBruger](
	[CHR] [int] NOT NULL,
	[BrugerID] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PKN_EjendomListBruger] PRIMARY KEY CLUSTERED 
(
	[CHR] ASC,
	[BrugerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
