﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Database
{
    public static class SqlDatabase
    {
        public static void Create(SqlConnection conn)
        {
            if (conn.State != System.Data.ConnectionState.Open)
                return;

            var basepath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            var filepath = Path.Combine(basepath, "Database");
            var files = Directory.GetFiles(filepath, "*.sql");

            var sql = new StringBuilder();
            sql.AppendLine($"USE [CHR_Data]");
            sql.AppendLine($"GO");
            sql.AppendLine($"SET ANSI_NULLS ON");
            sql.AppendLine($"GO");
            sql.AppendLine($"SET QUOTED_IDENTIFIER ON");
            sql.AppendLine($"GO");

            foreach (var file in files)
            {
                var lines = File.ReadAllLines(file);
                foreach (var line in lines)
                {
                    sql.AppendLine(line);
                }
            }

            var nn = sql.ToString();

            SqlUtils.ExecuteSql(conn, sql);
        }

        internal static void FillInData(SqlConnection conn)
        {
            // Fill in besætninger
            Program._info.Header = "CHR - Fill in besætninger";
            var sql = new StringBuilder();
            foreach (var item in Program._besætninger)
            {
                sql.AppendLine($"INSERT INTO [dbo].[Besaetning] SELECT {item.BeseatningsID}, {item.besaetningsNummer}, {item.dyreArtKode}, {item.brugsArtKode}, '{item.virksomhedsArtTekst}', {item.omsaetningsKode}, GETDATE(), GETDATE()");
            }
            var res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in BesaetningsStoerrelseType
            Program._info.Header = "CHR - Fill in BesaetningsStoerrelseType";
            sql = new StringBuilder();
            foreach (var item in Program._besætningstyper)
            {
                sql.AppendLine($"INSERT INTO [dbo].[BesaetningsStoerrelseType] SELECT {item.BeseatningsID}, {item.EjerID}, {item.BesaetningsNummer}, {item.DyreArtKode}, '{item.BesaetningsStoerrelseTekst.Replace("'", "''")}', {item.BesaetningsStoerrelse}, GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in Bruger
            Program._info.Header = "CHR - Fill in Bruger";
            sql = new StringBuilder();
            foreach (var item in Program._brugere)
            {
                sql.AppendLine($"INSERT INTO [dbo].[Bruger] SELECT {item.BrugerID}, {item.CVR}, '{item.Navn.Replace("'", "''")}', '{item.Adresse.Replace("'", "''")}', {item.PostNummer}, '{item.ByNavn.Replace("'", "''")}', '{item.PostDistrikt.Replace("'", "''")}', {item.KommuneNummer}, {(item.Reklamebeskyttelse == true ? 1 : 0)}, GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in brugerlistragtimestamdata
            Program._info.Header = "CHR - Fill in brugerlistragtimestamdata";
            sql = new StringBuilder();
            foreach (var item in Program._brugerlistragtimestamdata)
            {
                sql.AppendLine($"INSERT INTO [dbo].[BrugerListRagtimeStamdata] SELECT {item.TDC_ID}, {item.BrugerID}, {item.Status}, GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in brugsarter
            Program._info.Header = "CHR - Fill in brugsarter";
            sql = new StringBuilder();
            foreach (var item in Program._brugsarter)
            {
                sql.AppendLine($"INSERT INTO [dbo].[BrugsArt] SELECT {item.BrugsArtKode}, '{item.BrugsArtTekst.Replace("'", "''")}', GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in dyreart
            Program._info.Header = "CHR - Fill in dyreart";
            sql = new StringBuilder();
            foreach (var item in Program._dyreart)
            {
                sql.AppendLine($"INSERT INTO [dbo].[DyreArt] SELECT {item.DyreArtKode}, '{item.DyreArtTekst.Replace("'", "''")}', GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in ejendomme
            Program._info.Header = "CHR - Fill in ejendomme";
            sql = new StringBuilder();
            foreach (var item in Program._ejendomme)
            {
                sql.AppendLine($"INSERT INTO [dbo].[Ejendom] SELECT {item.CHR}, '{item.Adresse.Replace("'", "''")}', {item.PostNummer}, '{item.ByNavn.Replace("'", "''")}', '{item.PostDistrikt.Replace("'", "''")}', {item.KommuneNummer}, {item.StaldKoordinatX.ToString().Replace(",", ".")}, {item.StaldKoordinatY.ToString().Replace(",", ".")}, GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in ejendomlistbesaetning
            Program._info.Header = "CHR - Fill in ejendomlistbesaetning";
            sql = new StringBuilder();
            foreach (var item in Program._ejendomlistbesaetning)
            {
                sql.AppendLine($"INSERT INTO [dbo].[EjendomListBesaetning] SELECT {item.CHR}, {item.BeseatningsID}, {item.BesaetningsNummer}, GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in ejendomlistbruger
            Program._info.Header = "CHR - Fill in ejendomlistbruger";
            sql = new StringBuilder();
            foreach (var item in Program._ejendomlistbruger)
            {
                sql.AppendLine($"INSERT INTO [dbo].[EjendomListBruger] SELECT {item.CHR}, {item.BrugerID}, GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in ejendomlistdyrlaege
            Program._info.Header = "CHR - Fill in ejendomlistdyrlaege";
            sql = new StringBuilder();
            foreach (var item in Program._ejendomlistdyrlaege)
            {
                sql.AppendLine($"INSERT INTO [dbo].[EjendomListDyrlaege] SELECT {item.CHR}, {item.PraksisNr}, GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in ejendomlistejer
            Program._info.Header = "CHR - Fill in ejendomlistejer";
            sql = new StringBuilder();
            foreach (var item in Program._ejendomlistejer)
            {
                sql.AppendLine($"INSERT INTO [dbo].[EjendomListEjer] SELECT {item.EjerID}, {item.CHR}, GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in ejere
            Program._info.Header = "CHR - Fill in ejere";
            sql = new StringBuilder();
            foreach (var item in Program._ejere)
            {
                sql.AppendLine($"INSERT INTO [dbo].[Ejer] SELECT {item.EjerID}, {item.CVR}, '{item.Navn.Replace("'", "''")}', '{item.Adresse.Replace("'", "''")}', {item.PostNummer}, '{item.ByNavn.Replace("'", "''")}', '{item.PostDistrikt.Replace("'", "''")}', {item.KommuneNummer}, {(item.Reklamebeskyttelse == true ? 1 : 0)}, GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in ejerlistragtimestamdata
            Program._info.Header = "CHR - Fill in ejerlistragtimestamdata";
            sql = new StringBuilder();
            foreach (var item in Program._ejerlistragtimestamdata)
            {
                sql.AppendLine($"INSERT INTO [dbo].[EjerListRagtimeStamdata] SELECT {item.TDC_ID}, {item.EjerID}, {item.Status}, GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in kommuner
            Program._info.Header = "CHR - Fill in kommuner";
            sql = new StringBuilder();
            foreach (var item in Program._kommuner)
            {
                sql.AppendLine($"INSERT INTO [dbo].[Kommune] SELECT {item.KommuneNummer}, '{item.KommuneNavn.Replace("'", "''")}', GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in omsaetning
            Program._info.Header = "CHR - Fill in omsaetning";
            sql = new StringBuilder();
            foreach (var item in Program._omsaetning)
            {
                sql.AppendLine($"INSERT INTO [dbo].[Omsaetning] SELECT {item.OmsaetningsKode}, '{item.OmsaetningsTekst.Replace("'", "''")}', GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in sygdom
            Program._info.Header = "CHR - Fill in sygdom";
            sql = new StringBuilder();
            foreach (var item in Program._sygdom)
            {
                sql.AppendLine($"INSERT INTO [dbo].[Sygdom] SELECT '{item.SygdomsKode.Replace("'", "''")}', '{item.SygdomsTekst.Replace("'", "''")}', GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in sygdomsniveau
            Program._info.Header = "CHR - Fill in sygdomsniveau";
            sql = new StringBuilder();
            foreach (var item in Program._sygdomsniveau)
            {
                sql.AppendLine($"INSERT INTO [dbo].[SygdomsNiveau] SELECT {item.SygdomsNiveauKode}, '{item.SygdomsNiveauTekst.Replace("'", "''")}', GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in veterinaerehaendelser
            Program._info.Header = "CHR - Fill in veterinaerehaendelser";
            sql = new StringBuilder();
            foreach (var item in Program._veterinaerehaendelser)
            {
                sql.AppendLine($"INSERT INTO [dbo].[VeterinaereHaendelser] SELECT {item.CHR}, {(item.VeterinaereProblemer == true ? 1 : 0)}, GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in veterinaerehaendelse
            Program._info.Header = "CHR - Fill in veterinaerehaendelse";
            sql = new StringBuilder();
            foreach (var item in Program._veterinaerehaendelse)
            {
                sql.AppendLine($"INSERT INTO [dbo].[VeterinaerHaendelse] SELECT {item.ID}, {item.CHR}, {item.DyreArtKode}, '{item.SygdomsKode.Replace("'", "''")}', '{item.VeterinaerStatusKode.Replace("'", "''")}', {item.SygdomsNiveauKode}, '{item.DatoVeterinaerStatus.ToString("yyyy-MM-dd")}', GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

            // Fill in veterinaerstatus
            Program._info.Header = "CHR - Fill in veterinaerstatus";
            sql = new StringBuilder();
            foreach (var item in Program._veterinaerstatus)
            {
                sql.AppendLine($"INSERT INTO [dbo].[VeterinaerStatus] SELECT '{item.VeterinaerStatusKode}', '{item.VeterinaerStatusTekst.Replace("'", "''")}', GETDATE(), GETDATE()");
            }
            res = SqlUtils.ExecuteSql(conn, sql);
            if (res < 0)
                return;

        }
    }
}
