﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    public class EjendomListBesaetning
    {
        public int CHR { get; set; }
        public int BeseatningsID { get; set; }
        public long BesaetningsNummer { get; set; }
    }
}
