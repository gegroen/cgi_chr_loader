IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SygdomsNiveau]') AND type in (N'U'))
DROP TABLE [dbo].[SygdomsNiveau]
GO

CREATE TABLE [dbo].[SygdomsNiveau](
	[SygdomsNiveauKode] [int] NOT NULL,
	[SygdomsNiveauTekst] [nvarchar](128) NOT NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PKN_SygdomsNiveau] PRIMARY KEY CLUSTERED 
(
	[SygdomsNiveauKode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
