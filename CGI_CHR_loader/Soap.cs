﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;

namespace CGI_CHR_loader
{
    public static class Soap
    {
        /// <!-- =================================================== -->
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputArray"></param>
        /// <returns></returns>
        /// <!-- =================================================== -->
        static String ByteArrayToString(byte[] inputArray)
        {
            StringBuilder output = new StringBuilder("");
            for (int i = 0; i < inputArray.Length; i++)
            {
                output.Append(inputArray[i].ToString("X2"));
            }
            return output.ToString();
        }

        /// <!-- =================================================== -->
        /// <summary>
        /// 
        /// </summary>
        /// <param name="phrase"></param>
        /// <returns></returns>
        /// <!-- =================================================== -->
        public static String SHA1Encrypt(String phrase)
        {
            UTF8Encoding encoder = new UTF8Encoding();
            SHA1CryptoServiceProvider sha1Hasher = new SHA1CryptoServiceProvider();
            byte[] hashedDataBytes = sha1Hasher.ComputeHash(encoder.GetBytes(phrase));
            return ByteArrayToString(hashedDataBytes);
        }

        /// <!-- =================================================== -->
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="element"></param>
        /// <param name="xnsm"></param>
        /// <returns></returns>
        /// <!-- =================================================== -->
        public static string GetXmlStringValue(XPathNavigator node, string element, XmlNamespaceManager xnsm)
        {
            string sRet = "";

            XPathNavigator sn = node.SelectSingleNode(element, xnsm);
            if (sn != null)
                sRet = sn.Value;

            return sRet;
        }

        /// <!-- =================================================== -->
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="element"></param>
        /// <param name="xnsm"></param>
        /// <returns></returns>
        /// <!-- =================================================== -->
        public static DateTime GetXmlDateTimeValue(XPathNavigator node, string element, XmlNamespaceManager xnsm)
        {
            DateTime dtRet = new DateTime(1900, 1, 1);

            XPathNavigator sn = node.SelectSingleNode(element, xnsm);
            if (sn != null)
            {
                DateTime.TryParse(sn.Value, out dtRet);
            }

            return dtRet;
        }

        /// <!-- =================================================== -->
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="element"></param>
        /// <param name="xnsm"></param>
        /// <returns></returns>
        /// <!-- =================================================== -->
        public static int GetXmlIntValue(XPathNavigator node, string element, XmlNamespaceManager xnsm)
        {
            int sRet = 0;

            XPathNavigator sn = node.SelectSingleNode(element, xnsm);
            if (sn != null)
            {
                int.TryParse(sn.Value, out sRet);
            }
            return sRet;
        }

        /// <!-- =================================================== -->
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="element"></param>
        /// <param name="xnsm"></param>
        /// <returns></returns>
        /// <!-- =================================================== -->
        public static long GetXmlLongValue(XPathNavigator node, string element, XmlNamespaceManager xnsm)
        {
            long sRet = 0;

            XPathNavigator sn = node.SelectSingleNode(element, xnsm);
            if (sn != null)
            {
                long.TryParse(sn.Value, out sRet);
            }
            return sRet;
        }

        /// <!-- =================================================== -->
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="element"></param>
        /// <param name="xnsm"></param>
        /// <returns></returns>
        /// <!-- =================================================== -->
        public static float GetXmlFloatValue(XPathNavigator node, string element, XmlNamespaceManager xnsm)
        {
            float sRet = 0;

            XPathNavigator sn = node.SelectSingleNode(element, xnsm);
            if (sn != null)
            {
                float.TryParse(sn.Value, out sRet);
            }
            return sRet;
        }

        public static decimal GetXmlDecimalValue(XPathNavigator node, string element, XmlNamespaceManager xnsm)
        {
            decimal sRet = 0;

            XPathNavigator sn = node.SelectSingleNode(element, xnsm);
            if (sn != null)
            {
                decimal.TryParse(sn.Value, out sRet);
            }
            return sRet;
        }

        /// <!-- =================================================== -->
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="element"></param>
        /// <param name="xnsm"></param>
        /// <returns></returns>
        /// <!-- =================================================== -->
        public static bool GetXmlBoolValue(XPathNavigator node, string element, XmlNamespaceManager xnsm)
        {
            bool sRet = false;

            XPathNavigator sn = node.SelectSingleNode(element, xnsm);
            if (sn != null)
            {
                sRet = (sn.Value.Equals("J", StringComparison.InvariantCultureIgnoreCase) ||
                    sn.Value.Equals("JA", StringComparison.InvariantCultureIgnoreCase) ||
                    sn.Value.Equals("Y", StringComparison.InvariantCultureIgnoreCase) ||
                    sn.Value.Equals("YES", StringComparison.InvariantCultureIgnoreCase) ||
                    sn.Value.Equals("T", StringComparison.InvariantCultureIgnoreCase) ||
                    sn.Value.Equals("TRUE", StringComparison.InvariantCultureIgnoreCase) ||
                    sn.Value.Equals("1", StringComparison.InvariantCultureIgnoreCase)
                    );
            }
            return sRet;
        }

        /// <!-- =================================================== -->
        /// <summary>
        /// 
        /// </summary>
        /// <param name="webrequest"></param>
        /// <param name="soapenvelope"></param>
        /// <param name="soapsecurity"></param>
        /// <param name="line"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        /// <!-- =================================================== -->
        public static XmlDocument ReadWebService(string webrequest, string soapenvelope, string soapsecurity, string line)
        {
            int MaxTry = 4;
            XmlDocument doc = null;

            while (MaxTry > 0)
            {
                HttpWebRequest request = HttpWebRequest.Create(webrequest) as HttpWebRequest;
                var soapEnvelope = soapenvelope.Replace("${SoapSecurity}", soapsecurity);
                soapEnvelope = soapEnvelope.Replace("zlinez", line);
                request.ContentType = "text/xml;charset=\"utf-8\"";
                request.Accept = "text/xml";
                request.Method = "POST";
                request.ContentLength = soapEnvelope.Length;
                doc = null;
                StreamReader reader = null;
                WebResponse response = null;

                try
                {
                    Stream requestStream = request.GetRequestStream();
                    using (StreamWriter streamWriter = new StreamWriter(requestStream))
                    {
                        streamWriter.Write(soapEnvelope);
                    }

                    response = request.GetResponse();
                    Stream responseStream = response.GetResponseStream();

                    reader = new StreamReader(responseStream ?? throw new InvalidOperationException());
                    string result = reader.ReadToEnd();

                    doc = new XmlDocument();
                    doc.LoadXml(result);
                    doc.PreserveWhitespace = true;
                    MaxTry = 0;
                }
                catch (Exception ex)
                {
                    MaxTry--;
                    if (MaxTry == 0)
                    {
                    }
                }
                finally
                {
                    reader?.Close();
                    response?.Close();
                }
            }
            return doc;
        }
    }
}
