﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    public class BesætningsType
    {
        public int BeseatningsID { get; set; }
        public int EjerID { get; set; }
        public long BesaetningsNummer { get; set; }
        public int DyreArtKode { get; set; }
        public string BesaetningsStoerrelseTekst { get; set; }
        public int BesaetningsStoerrelse { get; set; }
    }
}
