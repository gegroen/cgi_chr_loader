IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EjendomListEjer]') AND type in (N'U'))
DROP TABLE [dbo].[EjendomListEjer]
GO

CREATE TABLE [dbo].[EjendomListEjer](
	[EjerID] [int] NOT NULL,
	[CHR] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PKN_EjendomListEjer] PRIMARY KEY CLUSTERED 
(
	[EjerID] ASC,
	[CHR] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
