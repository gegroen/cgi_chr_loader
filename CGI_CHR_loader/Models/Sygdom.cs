﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    public class Sygdom
    {
        public string SygdomsKode { get; set; }
        public string SygdomsTekst { get; set; }
    }
}
