IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BesaetningsStoerrelseType]') AND type in (N'U'))
DROP TABLE [dbo].[BesaetningsStoerrelseType]
GO

CREATE TABLE [dbo].[BesaetningsStoerrelseType](
	[BeseatningsID] [int] NOT NULL,
	[EjerID] [int] NOT NULL,
	[BesaetningsNummer] [int] NOT NULL,
	[DyreArtKode] [int] NOT NULL,
	[BesaetningsStoerrelseTekst] [nvarchar](50) NOT NULL,
	[BesaetningsStoerrelse] [int] NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PKN_BesaetningsStoerrelseType] PRIMARY KEY CLUSTERED 
(
	[BeseatningsID] ASC,
	[BesaetningsNummer] ASC,
	[DyreArtKode] ASC,
	[BesaetningsStoerrelseTekst] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
