﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    public class BrugsArt
    {
        public int BrugsArtKode { get; set; }
        public string BrugsArtTekst { get; set; }
    }
}
