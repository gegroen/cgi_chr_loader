IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EjendomListBesaetning]') AND type in (N'U'))
DROP TABLE [dbo].[EjendomListBesaetning]
GO

CREATE TABLE [dbo].[EjendomListBesaetning](
	[CHR] [int] NOT NULL,
	[BeseatningsID] [int] NOT NULL,
	[BesaetningsNummer] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PKN_EjendomListBesaetning] PRIMARY KEY CLUSTERED 
(
	[CHR] ASC,
	[BeseatningsID] ASC,
	[BesaetningsNummer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
