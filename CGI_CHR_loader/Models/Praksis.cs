﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    public class Praksis
    {
        public int PraksisNr { get; set; }
        public string Navn { get; set; }
        public string Adresse { get; set; }
        public int PostNummer { get; set; }
        public string ByNavn { get; set; }
        public string PostDistrikt { get; set; }
    }
}
