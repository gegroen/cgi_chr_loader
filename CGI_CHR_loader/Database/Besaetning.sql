IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Besaetning]') AND type in (N'U'))
DROP TABLE [dbo].[Besaetning]
GO

CREATE TABLE [dbo].[Besaetning](
	[BeseatningsID] [int] NOT NULL,
	[BesaetningsNummer] [int] NOT NULL,
	[DyreArtKode] [int] NOT NULL,
	[BrugsArtKode] [int] NOT NULL,
	[VirksomhedsArtTekst] [nvarchar](128) NULL,
	[OmsaetningsKode] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PKN_Besaetning] PRIMARY KEY CLUSTERED 
(
	[BeseatningsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
