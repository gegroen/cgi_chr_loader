IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EjendomListDyrlaege]') AND type in (N'U'))
DROP TABLE [dbo].[EjendomListDyrlaege]
GO

CREATE TABLE [dbo].[EjendomListDyrlaege](
	[CHR] [int] NOT NULL,
	[PraksisNr] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PKN_EjendomListDyrlaege] PRIMARY KEY CLUSTERED 
(
	[CHR] ASC,
	[PraksisNr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
