﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader
{
    public static class SqlUtils
    {
        public static int ExecuteSql(SqlConnection conn, StringBuilder sql)
        {
            int iRet = 0;

            var aaa = sql.ToString();

            try
            {
                var lines = sql.ToString().Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                var exsql = new StringBuilder();
                foreach (var line in lines)
                {
                    if (line.Trim().Equals("GO"))
                    {
                        using (var cmd = new SqlCommand(exsql.ToString(), conn))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandTimeout = (60 * 60) * 24; // 24 timer

                            var result = cmd.BeginExecuteNonQuery();

                            cmd.EndExecuteNonQuery(result); //  cmd.ExecuteNonQuery();
                        }
                        exsql = new StringBuilder();
                    }
                    else
                    {
                        exsql.AppendLine(line);
                    }
                }
                if (exsql.Length > 0)
                {
                    using (var cmd = new SqlCommand(exsql.ToString(), conn))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = (60 * 60) * 24; // 24 timer

                        var result = cmd.BeginExecuteNonQuery();

                        cmd.EndExecuteNonQuery(result); //  cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                var msg = $"Error ({ex.Number}): {ex.Message}";
                Program._info.LatestError = msg;
               return -1;
            }
            catch (InvalidOperationException ex)
            {
                var msg = $"Error: ({ex.Message}";
                Program._info.LatestError = msg;
                return -1;
            }
            catch (Exception ex)
            {
                var msg = $"Error: ({ex.Message}";
                Program._info.LatestError = msg;
                return -1;
            }

            return iRet;
        }

    }
}
