IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EjerListRagtimeStamdata]') AND type in (N'U'))
DROP TABLE [dbo].[EjerListRagtimeStamdata]
GO

CREATE TABLE [dbo].[EjerListRagtimeStamdata](
	[TDC_ID] [decimal](13, 0) NOT NULL,
	[EjerID] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PKN_EjerListRagtimeStamdata] PRIMARY KEY CLUSTERED 
(
	[TDC_ID] ASC,
	[EjerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
