﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    public class SygdomsNiveau
    {
        public int SygdomsNiveauKode { get; set; }
        public string SygdomsNiveauTekst { get; set; }
    }
}
