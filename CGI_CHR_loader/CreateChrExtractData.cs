﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CGI_CHR_loader.Models;

namespace CGI_CHR_loader
{
    public static class CreateChrExtractData
    {
        private static char[] _trimchars = { ' ', ',', '.', '\t' };

        internal static void ExpandTable()
        {

            Program._info.BarColor = ConsoleColor.Blue;
            Program._info.Spinner = false;

            var fieldsnames =
            (from besætningstype in Program._besætningstyper
             join dyreart in Program._dyreart on besætningstype.DyreArtKode equals dyreart.DyreArtKode
             select NormaliseText(dyreart.DyreArtTekst) + "_" + NormaliseText(besætningstype.BesaetningsStoerrelseTekst)
            ).Distinct()?.ToList();


            var fieldNames = new List<string>();

            {
                var index = 0;
                Program._lookupByName.Add("EjerId", index); Program._lookupByIndex.Add(index++, "EjerId");
                Program._lookupByName.Add("CVR", index); Program._lookupByIndex.Add(index++, "CVR");
                Program._lookupByName.Add("TDC_ID", index); Program._lookupByIndex.Add(index++, "TDC_ID");
                Program._lookupByName.Add("Navn", index); Program._lookupByIndex.Add(index++, "Navn");
                Program._lookupByName.Add("Adresse", index); Program._lookupByIndex.Add(index++, "Adresse");
                Program._lookupByName.Add("Postnummer", index); Program._lookupByIndex.Add(index++, "Postnummer");
                Program._lookupByName.Add("ByNavn", index); Program._lookupByIndex.Add(index++, "ByNavn");
                Program._lookupByName.Add("dato_besaetning_stoerrelse", index); Program._lookupByIndex.Add(index++, "dato_besaetning_stoerrelse");

                Program._lookupByName.Add("Chr_nummer_1", index); Program._lookupByIndex.Add(index++, "Chr_nummer_1");
                Program._lookupByName.Add("Chr_nummer_2", index); Program._lookupByIndex.Add(index++, "Chr_nummer_2");
                Program._lookupByName.Add("Chr_nummer_3", index); Program._lookupByIndex.Add(index++, "Chr_nummer_3");
                Program._lookupByName.Add("Chr_nummer_4", index); Program._lookupByIndex.Add(index++, "Chr_nummer_4");
                Program._lookupByName.Add("Chr_nummer_5", index); Program._lookupByIndex.Add(index++, "Chr_nummer_5");
                Program._lookupByName.Add("Chr_nummer_6", index); Program._lookupByIndex.Add(index++, "Chr_nummer_6");
                Program._lookupByName.Add("Chr_nummer_7", index); Program._lookupByIndex.Add(index++, "Chr_nummer_7");
                Program._lookupByName.Add("Chr_nummer_8", index); Program._lookupByIndex.Add(index++, "Chr_nummer_8");
                Program._lookupByName.Add("Chr_nummer_9", index); Program._lookupByIndex.Add(index++, "Chr_nummer_9");
                Program._lookupByName.Add("Chr_nummer_10", index); Program._lookupByIndex.Add(index++, "Chr_nummer_10");

                Program._lookupByName.Add("Virk_art_nummer_1", index); Program._lookupByIndex.Add(index++, "Virk_art_nummer_1");
                Program._lookupByName.Add("Virk_art_nummer_2", index); Program._lookupByIndex.Add(index++, "Virk_art_nummer_2");
                Program._lookupByName.Add("Virk_art_nummer_3", index); Program._lookupByIndex.Add(index++, "Virk_art_nummer_3");
                Program._lookupByName.Add("Virk_art_nummer_4", index); Program._lookupByIndex.Add(index++, "Virk_art_nummer_4");
                Program._lookupByName.Add("Virk_art_nummer_5", index); Program._lookupByIndex.Add(index++, "Virk_art_nummer_5");

                Program._lookupByName.Add("Virk_art_nummer_1_Tekst", index); Program._lookupByIndex.Add(index++, "Virk_art_nummer_1_Tekst");
                Program._lookupByName.Add("Virk_art_nummer_2_Tekst", index); Program._lookupByIndex.Add(index++, "Virk_art_nummer_2_Tekst");
                Program._lookupByName.Add("Virk_art_nummer_3_Tekst", index); Program._lookupByIndex.Add(index++, "Virk_art_nummer_3_Tekst");
                Program._lookupByName.Add("Virk_art_nummer_4_Tekst", index); Program._lookupByIndex.Add(index++, "Virk_art_nummer_4_Tekst");
                Program._lookupByName.Add("Virk_art_nummer_5_Tekst", index); Program._lookupByIndex.Add(index++, "Virk_art_nummer_5_Tekst");

                foreach (var fieldsname in fieldsnames)
                {
                    Program._lookupByName.Add(fieldsname, index); Program._lookupByIndex.Add(index++, fieldsname);
                }

            }

            var ejerWithStamdata =
                (from ejer in Program._ejere
                 join stamdata in Program._ejerlistragtimestamdata on ejer.EjerID equals stamdata.EjerID
                 select new { EjerId = ejer.EjerID, CVR = ejer.CVR, TDC_ID = stamdata.TDC_ID, Navn = ejer.Navn, Adresse = ejer.Adresse, PostNr = ejer.PostNummer, PostDistrikt = ejer.PostDistrikt }
                ).Distinct()?.ToList();

            Program._info.Max = ejerWithStamdata.Count;
            Program._info.Value = 0;
            foreach (var item in ejerWithStamdata)
            {
                Program._info.Value++;
                Program._info.Report();

                List<VirkArt> virkList = new List<VirkArt>();
                List<BesaetningsType> besaetningsstoerrelse = new List<BesaetningsType>();

                // Get list of CHR for Owner
                // Hent alle ejendomme og besætninger
                var chr =
                    (from ejendomlistejer in Program._ejendomlistejer
                     join ejer in Program._ejere on ejendomlistejer.EjerID equals ejer.EjerID
                     where ejer.EjerID == item.EjerId
                     select ejendomlistejer.CHR
                     ).Distinct()?.ToList();
                var fieldtyper =
                    (from ejendom in Program._ejendomme
                     join ejendomlistejer in Program._ejendomlistejer on ejendom.CHR equals ejendomlistejer.CHR
                     join ejendomlistbesaetning in Program._ejendomlistbesaetning on ejendom.CHR equals ejendomlistbesaetning.CHR
                     join besætning in Program._besætninger on ejendomlistbesaetning.BeseatningsID equals besætning.BeseatningsID
                     join besaetningsstoerrelsetype in Program._besætningstyper
                        on new { a = ejendomlistejer.EjerID, b = besætning.BeseatningsID }
                        equals new { a = besaetningsstoerrelsetype.EjerID, b = besaetningsstoerrelsetype.BeseatningsID }
                     join dyreart in Program._dyreart on besaetningsstoerrelsetype.DyreArtKode equals dyreart.DyreArtKode
                     where ejendomlistejer.EjerID == item.EjerId && besaetningsstoerrelsetype.EjerID == item.EjerId
                     select new
                     {
                         CHR = ejendomlistejer.CHR,
                         VirksomhedsArtTekst = besætning.virksomhedsArtTekst,
                         BesaetningsStoerrelseTekst = besaetningsstoerrelsetype.BesaetningsStoerrelseTekst,
                         BesaetningsStoerrelse = besaetningsstoerrelsetype.BesaetningsStoerrelse,
                         BrugsArtKode = besætning.brugsArtKode,
                         BesaetningsNummer = besaetningsstoerrelsetype.BesaetningsNummer,
                         DyreArtKode = dyreart.DyreArtKode,
                         DyreArtTekst = dyreart.DyreArtTekst,
                         EjerId = ejendomlistejer.EjerID
                     }
                    ).Distinct()?.ToList();

                foreach (var fieldtype in fieldtyper)
                {
                    Int32.TryParse($"{fieldtype.DyreArtKode}{fieldtype.BrugsArtKode}", out var nummer);
                    VirkArt va = new VirkArt(nummer, fieldtype.VirksomhedsArtTekst.Trim(_trimchars));
                    if (!virkList.Any(f => f.Nummer == va.Nummer && f.Tekst == va.Tekst))
                        virkList.Add(va);

                    var fieldName = string.Join("_", NormaliseText(fieldtype.DyreArtTekst), NormaliseText(fieldtype.BesaetningsStoerrelseTekst));
                    if (fieldName != "_")
                    {
                        BesaetningsType bt = new BesaetningsType(fieldtype.DyreArtKode, fieldtype.BesaetningsStoerrelse, fieldName);
                        if (!besaetningsstoerrelse.Any(f => f.DyreArtKode == bt.DyreArtKode && f.StoerrelseTekst == bt.StoerrelseTekst))
                        {
                            besaetningsstoerrelse.Add(bt);
                        }
                        else
                        {
                            BesaetningsType btf = besaetningsstoerrelse.Find(f => f.DyreArtKode == bt.DyreArtKode && f.StoerrelseTekst == bt.StoerrelseTekst);
                            if (btf != null)
                                btf.Stoerrelse += bt.Stoerrelse;
                        }
                    }
                }

                // Create and Add Row
                {
                    var row = new object[Program._lookupByName.Count];

                    // EjerId
                    row[0] = item.EjerId;

                    // CVR
                    row[1] = item.CVR;

                    // TDC_ID
                    row[2] = item.TDC_ID;

                    // Navn
                    row[3] = item.Navn;

                    // Adresse
                    row[4] = item.Adresse;

                    // Postnummer
                    row[5] = item.PostNr;

                    // ByNavn
                    row[6] = item.PostDistrikt;

                    // dato_besaetning_stoerrelse
                    row[7] = DateTime.Now;


                    // Add CHR numbers
                    if (chr.Any())
                    {
                        int imax = chr.Count > 10 ? 10 : chr.Count;
                        if (Program._lookupByName.TryGetValue($"Chr_nummer_1", out var index))
                        {
                            for (var i = 0; i < imax; i++)
                            {
                                row[index + i] = chr[i];
                            }
                        }
                    }

                    // add Virk_art_nummer
                    if (virkList.Any())
                    {
                        int imax = virkList.Count > 5 ? 5 : virkList.Count;
                        if (Program._lookupByName.TryGetValue($"Virk_art_nummer_1", out var index))
                        {
                            for (var i = 0; i < imax; i++)
                            {
                                row[index + i] = virkList[i].Nummer;
                            }
                        }
                        if (Program._lookupByName.TryGetValue($"Virk_art_nummer_1_Tekst", out index))
                        {
                            for (var i = 0; i < imax; i++)
                            {
                                row[index + i] = virkList[i].Tekst;
                            }
                        }
                    }

                    // Add Besætninger
                    if (besaetningsstoerrelse.Any())
                    {
                        foreach (var besætningstrr in besaetningsstoerrelse)
                        {
                            if (Program._lookupByName.TryGetValue($"{besætningstrr.StoerrelseTekst}", out var index))
                            {
                                row[index] = besætningstrr.Stoerrelse;
                            }
                        }
                    }
                    Program._rows.Add(row);
                }

            }

        }

        internal static void CreateChrExtractDataTable(SqlConnection conn)
        {
            var tablename = "CHRExtractData";
            var sql = new StringBuilder();

            sql.AppendLine($"IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{tablename}]') AND type in (N'U'))");
            sql.AppendLine($"DROP TABLE [dbo].[{tablename}]");
            sql.AppendLine($"GO");
            sql.AppendLine($"CREATE TABLE [dbo].[{tablename}](");
            sql.AppendLine($"   [EjerId] [int] NOT NULL,");
            sql.AppendLine($"   [CVR] [int] NOT NULL,");
            sql.AppendLine($"   [TDC_ID] [decimal](13, 0) NOT NULL,");
            sql.AppendLine($"   [Navn] [nvarchar](128) NULL,");
            sql.AppendLine($"   [Adresse] [nvarchar](128) NULL,");
            sql.AppendLine($"   [Postnummer] [int] NULL,");
            sql.AppendLine($"   [ByNavn] [nvarchar](128) NULL,");
            sql.AppendLine($"   [dato_besaetning_stoerrelse] [datetime] NOT NULL,");

            for (var i = 8; i < Program._lookupByName.Count; i++)
            {
                Program._lookupByIndex.TryGetValue(i, out var name);

                if (i >= 23 && i <= 27)
                    sql.AppendLine($"   [{name}] [nvarchar](128) NULL,");
                else
                    sql.AppendLine($"   [{name}] [int] NULL,");
            }

            sql.AppendLine($"   CONSTRAINT [PKN_{tablename}] PRIMARY KEY CLUSTERED");
            sql.AppendLine($"   (");
            sql.AppendLine($"   [EjerId] ASC,");
            sql.AppendLine($"   [CVR] ASC,");
            sql.AppendLine($"   [TDC_ID] ASC");
            sql.AppendLine($"   )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]");
            sql.AppendLine($") ON [PRIMARY]");
            sql.AppendLine($"GO");

            //var sqlconnectionstring = ConfigurationManager.ConnectionStrings["CHR_CONN"].ToString();
            //using (var conn = new SqlConnection(sqlconnectionstring))
            //{
            //    conn.Open();

            SqlUtils.ExecuteSql(conn, sql);

            var line = 1;
            sql = new StringBuilder();
            foreach (var row in Program._rows)
            {
                var colsql = new StringBuilder();
                colsql.Append($"INSERT INTO [CHR_Data].[dbo].[{tablename}] SELECT");
                foreach (var col in Program._lookupByIndex)
                {
                    var colpc = "";

                    if (row[col.Key] == null)
                    {
                        colpc = $"NULL";
                    }
                    else if (col.Key == 2)
                    {
                        colpc = $"{row[col.Key]}";
                    }
                    else if (col.Key == 3 || col.Key == 4 || col.Key == 6 || (col.Key >= 23 && col.Key <= 27))
                    {
                        if (row[col.Key] != null)
                            colpc = $"'{row[col.Key].ToString().Trim().Replace("'", "''")}'";
                    }
                    else if (col.Key == 7)
                    {
                        if (DateTime.TryParse(row[col.Key].ToString().Trim() as string, out var dt))
                        {
                            colpc = $"'{dt.Year:0000}-{dt.Month:00}-{dt.Day:00}'";
                        }
                        else
                        {
                            colpc = $"NULL";
                        }
                    }
                    else
                    {
                        colpc = $"{row[col.Key].ToString().Trim()}";
                    }

                    if (col.Key != 0)
                        colsql.Append($", {colpc}");
                    else
                        colsql.Append($" {colpc}");

                }

                sql.AppendLine(colsql.ToString());

                if ((line % 5000) == 0)
                {
                    sql.AppendLine("GO");
                }
            }

            SqlUtils.ExecuteSql(conn, sql);
            //}
        }

        private static string NormaliseText(string s)
        {
            string str = "";
            string[] items = s.Trim().Split(new Char[] { ' ', ',', '.', '\t' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string item in items)
            {
                str += item.First().ToString().ToUpper() + item.Substring(1);
            }
            str = str.Replace("æ", "ae").Replace("Æ", "Ae");
            str = str.Replace("ø", "oe").Replace("Ø", "Oe");
            str = str.Replace("å", "aa").Replace("Å", "Aa");

            return str;
        }
    }

    public class VirkArt
    {
        public int Nummer { get; set; }
        public string Tekst { get; set; }

        public VirkArt(int nummer, string tekst)
        {
            Nummer = nummer;
            Tekst = tekst;
        }
    }

    public class BesaetningsType
    {
        public int DyreArtKode { get; set; }
        public int Stoerrelse { get; set; }
        public string StoerrelseTekst { get; set; }

        public BesaetningsType(int dyreartkode, int stoerrelse, string stoerrelsetekst)
        {
            DyreArtKode = dyreartkode;
            Stoerrelse = stoerrelse;
            StoerrelseTekst = stoerrelsetekst;
        }
    }

}
