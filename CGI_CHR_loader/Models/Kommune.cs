﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    public class Kommune
    {
        public int KommuneNummer { get; set; }
        public string KommuneNavn { get; set; }
    }
}
