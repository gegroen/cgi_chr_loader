﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    public class Ejendom
    {
        public int CHR { get; set; }
        public string Adresse { get; set; }
        public string ByNavn { get; set; }
        public int PostNummer { get; set; }
        public string PostDistrikt { get; set; }
        public int KommuneNummer { get; set; }
        public string KommuneNavn { get; set; }
        public float StaldKoordinatX { get; set; }
        public float StaldKoordinatY { get; set; }
    }
}
