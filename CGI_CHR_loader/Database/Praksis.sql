IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Praksis]') AND type in (N'U'))
DROP TABLE [dbo].[Praksis]
GO

CREATE TABLE [dbo].[Praksis](
	[PraksisNr] [int] NOT NULL,
	[Navn] [nvarchar](128) NULL,
	[Adresse] [nvarchar](128) NULL,
	[Postnummer] [int] NULL,
	[ByNavn] [nvarchar](128) NULL,
	[PostDistrikt] [nvarchar](128) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
 CONSTRAINT [PKN_Praksis] PRIMARY KEY CLUSTERED 
(
	[PraksisNr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
