﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    public class VeterinaerStatus
    {
        public string VeterinaerStatusKode { get; set; }
        public string VeterinaerStatusTekst { get; set; }
    }
}
