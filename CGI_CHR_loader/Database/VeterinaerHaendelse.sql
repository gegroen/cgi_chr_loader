IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VeterinaerHaendelse]') AND type in (N'U'))
DROP TABLE [dbo].[VeterinaerHaendelse]
GO

CREATE TABLE [dbo].[VeterinaerHaendelse](
	[ID] [int] NOT NULL,
	[CHR] [int] NOT NULL,
	[DyreArtKode] [int] NOT NULL,
	[SygdomsKode] [nvarchar](50) NOT NULL,
	[VeterinaerStatusKode] [nvarchar](50) NOT NULL,
	[SygdomsNiveauKode] [int] NOT NULL,
	[DatoVeterinaerStatus] [datetime] NOT NULL,
	[Created] [datetime] NOT NULL,
	[Modified] [datetime] NOT NULL
) ON [PRIMARY]
GO
