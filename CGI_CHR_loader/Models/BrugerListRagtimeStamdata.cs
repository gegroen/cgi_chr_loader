﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    class BrugerListRagtimeStamdata
    {
        public decimal TDC_ID { get; set; }
        public int BrugerID { get; set; }
        public int Status { get; set; }
    }
}
