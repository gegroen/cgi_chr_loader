﻿using CGI_CHR_loader.Database;
using CGI_CHR_loader.Models;
using NNMBase.Logging;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using static NNMBase.MiscToolbox;

namespace CGI_CHR_loader
{
    class Program
    {
        #region declares
        public static InfoFrame _info;

        private static int _maxtry = 20;             // 4 gange
        private static int _sleeptime = 1000 * 10;  // 30 secconds

        private static string _username = "nnmarkedsdata";
        private static string _password = "RWJGVDQzTUM5Rw==";
        static string _currentlogFilePath;
        static string _appName;

        private static string[] kommunekoder = {
            "101","147","151","153","155","157","159","161","163","165",
            "167","169","173","175","183","185","187","190","201","210",
            "217","219","223","230","240","250","253","259","260","265",
            "269","270","306","316","320","326","329","330","336","340",
            "350","360","370","376","390","400","410","420","430","440",
            "450","461","479","480","482","492","510","530","540","550",
            "561","563","573","575","580","607","615","621","630","657",
            "661","665","671","706","707","710","727","730","740","741",
            "746","751","756","760","766","773","779","787","791","810",
            "813","820","825","840","846","849","851","860"
        };

        private static string[] akommunekoder = { "450", "461", "479", "480", "482", "492", "510", "530", "540", "550" };

        public static List<Ejendom> _ejendomme { get; set; }
        public static List<Besætning> _besætninger;
        public static List<Ejer> _ejere;
        public static List<Bruger> _brugere;
        public static List<Praksis> _praksis;
        public static List<BesætningsType> _besætningstyper;
        public static List<DyreArt> _dyreart;
        public static List<BrugsArt> _brugsarter;
        public static List<Omsaetning> _omsaetning;
        public static List<Kommune> _kommuner;
        public static List<Sygdom> _sygdom;
        public static List<VeterinaerStatus> _veterinaerstatus;
        public static List<SygdomsNiveau> _sygdomsniveau;
        public static List<VeterinaereHaendelse> _veterinaerehaendelse;
        public static List<VeterinaereHaendelser> _veterinaerehaendelser;
        public static List<EjendomListBesaetning> _ejendomlistbesaetning;
        public static List<EjendomListBruger> _ejendomlistbruger;
        public static List<EjendomListDyrlaege> _ejendomlistdyrlaege;
        public static List<EjendomListEjer> _ejendomlistejer;
        public static List<EjerListRagtimeStamdata> _ejerlistragtimestamdata;
        public static List<BrugerListRagtimeStamdata> _brugerlistragtimestamdata;

        public static List<object[]> _rows;
        public static Dictionary<string, int> _lookupByName;
        public static Dictionary<int, string> _lookupByIndex;


        private static bool _loadFromFile = false;
        private static string _filename = @"C:\temp\CHR_Load.dat";

        #endregion declares

        /// <summary>
        /// Mains the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// This element was inserted by gergro on machine BBIPC0551.09-08-2018.09:47
        static void Main(string[] args)
        {
            InitializeLog();

            Stopwatch sw = new Stopwatch();
            sw.Start();

            if (checkRequiredFields())
            {
                try
                {
                    NameValueCollection appConfig = ConfigurationManager.AppSettings;
                    LogWriter.GlobalInstance.WriteLine($"{_appName} starting with arguments: " + string.Join(" ", args));

                    LogWriter.GlobalInstance.BeginSection("App.config start");
                    LogWriter.GlobalInstance.WriteLine(File.ReadAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), $"{_appName}.exe.config")));
                    LogWriter.GlobalInstance.EndSection("App.config end");

                    LogNewSection("Run", () => Run(args));

                    LogWriter.GlobalInstance.WriteLine($"Elapsed Time: {sw.Elapsed:g}");
                    LogWriter.GlobalInstance.Dispose();
                    Email.SendMail($"OK<br/>Time: {sw.Elapsed:g}", $"Info - {_appName}", Email.MailStatus.Info, _currentlogFilePath);
                }
                catch (Exception ex)
                {
                    LogWriter.GlobalInstance.Dispose();
                    Email.SendMail(ex.ToString(), $"Error - {_appName}", Email.MailStatus.Error, _currentlogFilePath);
                    Console.WriteLine(ex);
                    Environment.Exit(1);
                }
            }
            sw.Stop();
            Console.WriteLine($"Done! {sw.Elapsed:g}");
            Environment.Exit(0);



        }

        /// <summary>
        /// Runs the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// This element was inserted by gergro on machine BBIPC0551.09-08-2018.09:47
        private static void Run(string[] args)
        {
            {
                _ejendomme = new List<Ejendom>();
                _besætninger = new List<Besætning>();
                _ejere = new List<Ejer>();
                _brugere = new List<Bruger>();
                _praksis = new List<Praksis>();
                _besætningstyper = new List<BesætningsType>();
                _dyreart = new List<DyreArt>();
                _kommuner = new List<Kommune>();
                _brugsarter = new List<BrugsArt>();
                _omsaetning = new List<Omsaetning>();
                _sygdom = new List<Sygdom>();
                _veterinaerstatus = new List<VeterinaerStatus>();
                _sygdomsniveau = new List<SygdomsNiveau>();
                _veterinaerehaendelse = new List<VeterinaereHaendelse>();
                _veterinaerehaendelser = new List<VeterinaereHaendelser>();
                _ejendomlistbesaetning = new List<EjendomListBesaetning>();
                _ejendomlistbruger = new List<EjendomListBruger>();
                _ejendomlistdyrlaege = new List<EjendomListDyrlaege>();
                _ejendomlistejer = new List<EjendomListEjer>();
                _ejerlistragtimestamdata = new List<EjerListRagtimeStamdata>();
                _brugerlistragtimestamdata = new List<BrugerListRagtimeStamdata>();
                _rows = new List<object[]>();
                _lookupByName = new Dictionary<string, int>();
                _lookupByIndex = new Dictionary<int, string>();
            }

            _info = new InfoFrame { Spinner = true, BarColor = ConsoleColor.Blue, Header = "CHR - Fødevarestyrelsen" };
            _info.Start();

            if (_loadFromFile)
            {
                Load(_filename);

                //_rows = new List<object[]>();
                //_lookupByName = new Dictionary<string, int>();
                //_lookupByIndex = new Dictionary<int, string>();

                //CreateChrExtractData.ExpandTable();
                //Save(_filename);

                var sqlconnectionstring = ConfigurationManager.ConnectionStrings["CHR_CONN"].ToString();
                using (var conn = new SqlConnection(sqlconnectionstring))
                {
                    conn.Open();
                    SqlDatabase.Create(conn);
                    SqlDatabase.FillInData(conn);
                    CreateChrExtractData.CreateChrExtractDataTable(conn);
                }

                _info.BarColor = ConsoleColor.DarkYellow;
                _info.Spinner = true;
            }
            else
            {
                var securityheader = GetSecurityHeader(_username, _password);
                _info.Info = "Hent Ejendomme";
                LogNewSection("LæsEjendomme", () => LæsEjendomme(securityheader));
                _info.Info = "Hent Besætninger";
                LogNewSection("LæsBesætninger", () => LæsBesætninger(securityheader));
                _info.Info = "Hent Vetrinære";
                LogNewSection("LæsVeterinæreHaendelser", () => LæsVeterinæreHaendelser(securityheader));

                _ejerlistragtimestamdata = MatchOwnerAgainstRagtime.Match(_ejere, _info).ToList();

                _brugerlistragtimestamdata = MatchUserAgainstRagtime.Match(_brugere, _info).ToList();

                LogNewSection("CreateChrExtractData.ExpandTable", () => CreateChrExtractData.ExpandTable());

                LogNewSection("Save", () => Save(_filename));

                // create table
                var sqlconnectionstring = ConfigurationManager.ConnectionStrings["CHR_CONN"].ToString();
                using (var conn = new SqlConnection(sqlconnectionstring))
                {
                    conn.Open();
                    SqlDatabase.Create(conn);
                    SqlDatabase.FillInData(conn);
                    CreateChrExtractData.CreateChrExtractDataTable(conn);
                }
            }

            _info.Stop();
            _info.Dispose();

        }

        /// <summary>
        /// Initializes the log.
        /// </summary>
        /// This element was inserted by gergro on machine BBIPC0551.06-08-2018.13:22
        private static void InitializeLog()
        {
            _appName = Path.GetFileNameWithoutExtension(AppDomain.CurrentDomain.FriendlyName);
            string logsPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Logs");
            Directory.CreateDirectory(logsPath);

            string logFileName = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".txt";
            _currentlogFilePath = Path.Combine(logsPath, logFileName);

            LogWriter.GlobalInstance = new LogWriter(new DefaultLogFormatter(), FileLogTarget.Initialize(_currentlogFilePath));
        }

        /// <summary>
        /// Logs the new section.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="func">The function.</param>
        /// This element was inserted by gergro on machine BBIPC0551.06-08-2018.13:22
        private static void LogNewSection(string name, Action func)
        {
            LogWriter.GlobalInstance.BeginSection(name + " start");
            func();
            LogWriter.GlobalInstance.EndSection(name + " end");
        }

        /// <summary>
        /// Loads the specified filename.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// This element was inserted by gergro on machine BBIPC0551.09-08-2018.09:48
        private static void Load(string filename)
        {
            using (var fileStream = File.OpenRead(filename))
            using (var reader = new BinaryReader(fileStream))
            {
                // Ejendomme
                var count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _ejendomme.Add(new Ejendom
                    {
                        Adresse = reader.ReadString(),
                        ByNavn = reader.ReadString(),
                        CHR = reader.ReadInt32(),
                        KommuneNavn = reader.ReadString(),
                        KommuneNummer = reader.ReadInt32(),
                        PostDistrikt = reader.ReadString(),
                        PostNummer = reader.ReadInt32(),
                        StaldKoordinatX = reader.ReadSingle(),
                        StaldKoordinatY = reader.ReadSingle()
                    });
                    _info.Ejendomme = _ejendomme.Count;
                    _info.Report();
                }
                
                // Besætninger
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _besætninger.Add(new Besætning
                    {
                        besaetningsNummer = reader.ReadInt64(),
                        BeseatningsID = reader.ReadInt32(),
                        brugsArtKode = reader.ReadInt32(),
                        brugsArtTekst = reader.ReadString(),
                        dyreArtKode = reader.ReadInt32(),
                        dyreArtTekst = reader.ReadString(),
                        omsaetningsKode = reader.ReadInt32(),
                        omsaetningsTekst = reader.ReadString(),
                        virksomhedsArtTekst = reader.ReadString()
                    });
                    _info.Besætninger = _besætninger.Count;
                    _info.Report();
                }

                // Ejere
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _ejere.Add(new Ejer
                    {
                        Adresse = reader.ReadString(),
                        ByNavn = reader.ReadString(),
                        CVR = reader.ReadInt32(),
                        KommuneNavn = reader.ReadString(),
                        KommuneNummer = reader.ReadInt32(),
                        Navn = reader.ReadString(),
                        EjerID = reader.ReadInt32(),
                        PostDistrikt = reader.ReadString(),
                        PostNummer = reader.ReadInt32(),
                        Reklamebeskyttelse = reader.ReadBoolean()
                    });
                    _info.Ejere = _ejere.Count;
                    _info.Report();
                }

                // Brugere
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _brugere.Add(new Bruger
                    {
                        Adresse = reader.ReadString(),
                        ByNavn = reader.ReadString(),
                        CVR = reader.ReadInt32(),
                        KommuneNavn = reader.ReadString(),
                        KommuneNummer = reader.ReadInt32(),
                        Navn = reader.ReadString(),
                        BrugerID = reader.ReadInt32(),
                        PostDistrikt = reader.ReadString(),
                        PostNummer = reader.ReadInt32(),
                        Reklamebeskyttelse = reader.ReadBoolean()
                    });
                    _info.Brugere = _brugere.Count;
                    _info.Report();
                }

                // Praksis
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _praksis.Add(new Praksis
                    {
                        Adresse = reader.ReadString(),
                        ByNavn = reader.ReadString(),
                        Navn = reader.ReadString(),
                        PostDistrikt = reader.ReadString(),
                        PostNummer = reader.ReadInt32(),
                        PraksisNr = reader.ReadInt32()
                    });
                    _info.Praksis = _praksis.Count;
                    _info.Report();
                }

                // BesætningsTyper
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _besætningstyper.Add(new BesætningsType
                    {
                        BesaetningsNummer = reader.ReadInt64(),
                        BesaetningsStoerrelse = reader.ReadInt32(),
                        BesaetningsStoerrelseTekst = reader.ReadString(),
                        BeseatningsID = reader.ReadInt32(),
                        DyreArtKode = reader.ReadInt32(),
                        EjerID = reader.ReadInt32()
                    });
                    _info.BesætningsTyper = _besætningstyper.Count;
                    _info.Report();
                }

                // DyreArt
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _dyreart.Add(new DyreArt
                    {
                        DyreArtKode = reader.ReadInt32(),
                        DyreArtTekst = reader.ReadString()
                    });
                    _info.Dyreart = _dyreart.Count;
                    _info.Report();
                }

                // Kommuner
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _kommuner.Add(new Kommune
                    {
                        KommuneNavn = reader.ReadString(),
                        KommuneNummer = reader.ReadInt32()
                    });
                    //_info.Kom = _kommuner.Count;
                    //_info.Report();
                }

                // BrugsArter
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _brugsarter.Add(new BrugsArt
                    {
                        BrugsArtKode = reader.ReadInt32(),
                        BrugsArtTekst = reader.ReadString()
                    });
                    _info.Brugasrter = _brugsarter.Count;
                    _info.Report();
                }

                // Omsætning
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _omsaetning.Add(new Omsaetning
                    {
                        OmsaetningsKode = reader.ReadInt32(),
                        OmsaetningsTekst = reader.ReadString()
                    });
                    _info.Omsætning = _omsaetning.Count;
                    _info.Report();
                }

                // Sygdom
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _sygdom.Add(new Sygdom
                    {
                        SygdomsKode = reader.ReadString(),
                        SygdomsTekst = reader.ReadString()
                    });
                    _info.Sygdom = _sygdom.Count;
                    _info.Report();
                }

                // VeterinaerStatus
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _veterinaerstatus.Add(new VeterinaerStatus
                    {
                        VeterinaerStatusKode = reader.ReadString(),
                        VeterinaerStatusTekst = reader.ReadString()
                    });
                    _info.VeterinaerStatus = _veterinaerstatus.Count;
                    _info.Report();
                }

                // SygdomsNiveau
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _sygdomsniveau.Add(new SygdomsNiveau
                    {
                        SygdomsNiveauKode = reader.ReadInt32(),
                        SygdomsNiveauTekst = reader.ReadString()
                    });
                    _info.SygdomsNiveau = _sygdomsniveau.Count;
                    _info.Report();
                }

                // VeterinaereHaendelse
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _veterinaerehaendelse.Add(new VeterinaereHaendelse
                    {
                        CHR = reader.ReadInt32(),
                        DatoVeterinaerStatus = DateTime.Parse(reader.ReadString()),
                        DyreArtKode =reader.ReadInt32(),
                        ID = reader.ReadInt32(),
                        SygdomsKode =reader.ReadString(),
                        SygdomsNiveauKode = reader.ReadInt32(),
                        VeterinaerStatusKode = reader.ReadString()
                    });
                    _info.VeterinaereHaendelse = _veterinaerehaendelse.Count;
                    _info.Report();
                }

                // VeterinaereHaendelser
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _veterinaerehaendelser.Add(new VeterinaereHaendelser
                    {
                        CHR = reader.ReadInt32(),
                        VeterinaereProblemer = reader.ReadBoolean()
                    });
                    _info.VeterinaereHaendelser = _veterinaerehaendelser.Count;
                    _info.Report();
                }

                // EjendomListBesaetning
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _ejendomlistbesaetning.Add(new EjendomListBesaetning
                    {
                        BesaetningsNummer = reader.ReadInt64(),
                        BeseatningsID = reader.ReadInt32(),
                        CHR = reader.ReadInt32()
                    });
                    _info.EjendomListBesaetning = _ejendomlistbesaetning.Count;
                    _info.Report();
                }

                // EjendomListBruger
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _ejendomlistbruger.Add(new EjendomListBruger
                    {
                        BrugerID = reader.ReadInt32(),
                        CHR = reader.ReadInt32()
                    });
                    _info.EjendomListBruger = _ejendomlistbruger.Count;
                    _info.Report();
                }

                // EjendomListDyrlaege
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _ejendomlistdyrlaege.Add(new EjendomListDyrlaege
                    {
                        CHR = reader.ReadInt32(),
                        PraksisNr = reader.ReadInt32()
                    });
                    _info.EjendomListDyrlaege = _ejendomlistdyrlaege.Count;
                    _info.Report();
                }

                // EjendomListEjer
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _ejendomlistejer.Add(new EjendomListEjer
                    {
                        CHR = reader.ReadInt32(),
                        EjerID = reader.ReadInt32()
                    });
                    _info.EjendomListEjer = _ejendomlistejer.Count;
                    _info.Report();
                }

                // EjerListRagtimeStamdata
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _ejerlistragtimestamdata.Add(new EjerListRagtimeStamdata
                    {
                        TDC_ID = reader.ReadDecimal(),
                        EjerID = reader.ReadInt32(),
                        Status = reader.ReadInt32()
                    });
                }

                // BrugerListRagtimeStamdata
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    _brugerlistragtimestamdata.Add(new BrugerListRagtimeStamdata
                    {
                        TDC_ID = reader.ReadDecimal(),
                        BrugerID = reader.ReadInt32(),
                        Status = reader.ReadInt32()
                    });
                }

                //
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    var name = reader.ReadString();
                    var index = reader.ReadInt32();

                    _lookupByName.Add(name, index);
                    _lookupByIndex.Add(index, name);
                }

                //
                count = reader.ReadInt32();
                for (int i = 0; i < count; i++)
                {
                    var cols = reader.ReadInt32();
                    var row = new object[cols];
                    for (int j = 0; j < cols; j++)
                    {
                        if (j == 2)
                        {
                            decimal.TryParse(reader.ReadString(), out decimal tdc_id);
                            row[j] = tdc_id;
                        }
                        else if (j == 3 || j == 4 || j == 6 || (j >= 23 && j <= 27 ))
                        {
                            row[j] = reader.ReadString();
                        }
                        else if (j == 7)
                        {
                            if (DateTime.TryParse(reader.ReadString(), out DateTime dato_besaetning_stoerrelse))
                            {
                                row[j] = dato_besaetning_stoerrelse;
                            }
                        }
                        else
                        {
                            int.TryParse(reader.ReadString(), out var value);
                            row[j] = value;
                        }
                    }
                    _rows.Add(row);
                }
            }
        }

        /// <summary>
        /// Saves the specified filename.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// This element was inserted by gergro on machine BBIPC0551.09-08-2018.09:48
        private static void Save(string filename)
        {
            using (var fileStream = File.Create(filename))
            using (var writer = new BinaryWriter(fileStream))
            {
                // Ejendomme
                writer.Write(_ejendomme.Count);
                foreach (var item in _ejendomme)
                {
                    writer.Write(item.Adresse == null ? "" : item.Adresse);
                    writer.Write(item.ByNavn == null ? "" : item.ByNavn);
                    writer.Write(item.CHR);
                    writer.Write(item.KommuneNavn == null ? "" : item.KommuneNavn);
                    writer.Write(item.KommuneNummer);
                    writer.Write(item.PostDistrikt == null ? "" : item.PostDistrikt);
                    writer.Write(item.PostNummer);
                    writer.Write(item.StaldKoordinatX);
                    writer.Write(item.StaldKoordinatY);
                }

                // Besætninger
                writer.Write(_besætninger.Count);
                foreach (var item in _besætninger)
                {
                    writer.Write(item.besaetningsNummer);
                    writer.Write(item.BeseatningsID);
                    writer.Write(item.brugsArtKode);
                    writer.Write(item.brugsArtTekst == null ? "" : item.brugsArtTekst);
                    writer.Write(item.dyreArtKode);
                    writer.Write(item.dyreArtTekst == null ? "" : item.dyreArtTekst);
                    writer.Write(item.omsaetningsKode);
                    writer.Write(item.omsaetningsTekst == null ? "" : item.omsaetningsTekst);
                    writer.Write(item.virksomhedsArtTekst == null ? "" : item.virksomhedsArtTekst);
                }

                // Ejere
                writer.Write(_ejere.Count);
                foreach (var item in _ejere)
                {
                    writer.Write(item.Adresse == null ? "" : item.Adresse);
                    writer.Write(item.ByNavn == null ? "" : item.ByNavn);
                    writer.Write(item.CVR);
                    writer.Write(item.KommuneNavn == null ? "" : item.KommuneNavn);
                    writer.Write(item.KommuneNummer);
                    writer.Write(item.Navn == null ? "" : item.Navn);
                    writer.Write(item.EjerID);
                    writer.Write(item.PostDistrikt == null ? "" : item.PostDistrikt);
                    writer.Write(item.PostNummer);
                    writer.Write(item.Reklamebeskyttelse);
                }

                // Brugere
                writer.Write(_brugere.Count);
                foreach (var item in _brugere)
                {
                    writer.Write(item.Adresse == null ? "" : item.Adresse);
                    writer.Write(item.ByNavn == null ? "" : item.ByNavn);
                    writer.Write(item.CVR);
                    writer.Write(item.KommuneNavn == null ? "" : item.KommuneNavn);
                    writer.Write(item.KommuneNummer);
                    writer.Write(item.Navn == null ? "" : item.Navn);
                    writer.Write(item.BrugerID);
                    writer.Write(item.PostDistrikt == null ? "" : item.PostDistrikt);
                    writer.Write(item.PostNummer);
                    writer.Write(item.Reklamebeskyttelse);
                }

                // Praksis
                writer.Write(_praksis.Count);
                foreach (var item in _praksis)
                {
                    writer.Write(item.Adresse == null ? "" : item.Adresse);
                    writer.Write(item.ByNavn == null ? "" : item.ByNavn);
                    writer.Write(item.Navn == null ? "" : item.Navn);
                    writer.Write(item.PostDistrikt == null ? "" : item.PostDistrikt);
                    writer.Write(item.PostNummer);
                    writer.Write(item.PraksisNr);
                }

                // BesætningsTyper
                writer.Write(_besætningstyper.Count);
                foreach (var item in _besætningstyper)
                {
                    writer.Write(item.BesaetningsNummer);
                    writer.Write(item.BesaetningsStoerrelse);
                    writer.Write(item.BesaetningsStoerrelseTekst == null ? "" : item.BesaetningsStoerrelseTekst);
                    writer.Write(item.BeseatningsID);
                    writer.Write(item.DyreArtKode);
                    writer.Write(item.EjerID);
                }

                // DyreArt
                writer.Write(_dyreart.Count);
                foreach (var item in _dyreart)
                {
                    writer.Write(item.DyreArtKode);
                    writer.Write(item.DyreArtTekst == null ? "" : item.DyreArtTekst);
                }

                // Kommuner
                writer.Write(_kommuner.Count);
                foreach (var item in _kommuner)
                {
                    writer.Write(item.KommuneNavn == null ? "" : item.KommuneNavn);
                    writer.Write(item.KommuneNummer);
                }

                // BrugsArter
                writer.Write(_brugsarter.Count);
                foreach (var item in _brugsarter)
                {
                    writer.Write(item.BrugsArtKode);
                    writer.Write(item.BrugsArtTekst == null ? "" : item.BrugsArtTekst);
                }

                // Omsætning
                writer.Write(_omsaetning.Count);
                foreach (var item in _omsaetning)
                {
                    writer.Write(item.OmsaetningsKode);
                    writer.Write(item.OmsaetningsTekst == null ? "" : item.OmsaetningsTekst);
                }
                
                // Sygdom
                writer.Write(_sygdom.Count);
                foreach (var item in _sygdom)
                {
                    writer.Write(item.SygdomsKode == null ? "" : item.SygdomsKode);
                    writer.Write(item.SygdomsTekst == null ? "" : item.SygdomsTekst);
                }
                
                // VeterinaerStatus
                writer.Write(_veterinaerstatus.Count);
                foreach (var item in _veterinaerstatus)
                {
                    writer.Write(item.VeterinaerStatusKode == null ? "" : item.VeterinaerStatusKode);
                    writer.Write(item.VeterinaerStatusTekst == null ? "" : item.VeterinaerStatusTekst);
                }

                // SygdomsNiveau
                writer.Write(_sygdomsniveau.Count);
                foreach (var item in _sygdomsniveau)
                {
                    writer.Write(item.SygdomsNiveauKode);
                    writer.Write(item.SygdomsNiveauTekst == null ? "" : item.SygdomsNiveauTekst);
                }

                // VeterinaereHaendelse
                writer.Write(_veterinaerehaendelse.Count);
                foreach (var item in _veterinaerehaendelse)
                {
                    writer.Write(item.CHR);
                    writer.Write(item.DatoVeterinaerStatus.ToString("yyyy-MM-dd"));
                    writer.Write(item.DyreArtKode);
                    writer.Write(item.ID);
                    writer.Write(item.SygdomsKode == null ? "" : item.SygdomsKode);
                    writer.Write(item.SygdomsNiveauKode);
                    writer.Write(item.VeterinaerStatusKode == null ? "" : item.VeterinaerStatusKode);
                }

                // VeterinaereHaendelser
                writer.Write(_veterinaerehaendelser.Count);
                foreach (var item in _veterinaerehaendelser)
                {
                    writer.Write(item.CHR);
                    writer.Write(item.VeterinaereProblemer);
                }

                // EjendomListBesaetning
                writer.Write(_ejendomlistbesaetning.Count);
                foreach (var item in _ejendomlistbesaetning)
                {
                    writer.Write(item.BesaetningsNummer);
                    writer.Write(item.BeseatningsID);
                    writer.Write(item.CHR);
                }

                // EjendomListBruger
                writer.Write(_ejendomlistbruger.Count);
                foreach (var item in _ejendomlistbruger)
                {
                    writer.Write(item.BrugerID);
                    writer.Write(item.CHR);
                }

                // EjendomListDyrlaege
                writer.Write(_ejendomlistdyrlaege.Count);
                foreach (var item in _ejendomlistdyrlaege)
                {
                    writer.Write(item.CHR);
                    writer.Write(item.PraksisNr);
                }

                // EjendomListEjer
                writer.Write(_ejendomlistejer.Count);
                foreach (var item in _ejendomlistejer)
                {
                    writer.Write(item.CHR);
                    writer.Write(item.EjerID);
                }

                // EjerListRagtimeStamdata
                writer.Write(_ejerlistragtimestamdata.Count);
                foreach (var item in _ejerlistragtimestamdata)
                {
                    writer.Write(item.TDC_ID);
                    writer.Write(item.EjerID);
                    writer.Write(item.Status);
                }

                // BrugerListRagtimeStamdata
                writer.Write(_brugerlistragtimestamdata.Count);
                foreach (var item in _brugerlistragtimestamdata)
                {
                    writer.Write(item.TDC_ID);
                    writer.Write(item.BrugerID);
                    writer.Write(item.Status);
                }

                //
                writer.Write(_lookupByName.Count);
                foreach (var item in _lookupByName)
                {
                    writer.Write(item.Key);
                    writer.Write(item.Value);
                }

                //
                writer.Write(_rows.Count);
                foreach (var row in _rows)
                {
                    writer.Write(row.Count());
                    foreach (var col in row)
                    {
                        writer.Write(col == null ? "" : $"{col}");
                    }
                }

            }
        }

        /// <summary>
        /// LÆSs the ejendomme.
        /// </summary>
        /// <param name="soapSecurity">The SOAP security.</param>
        /// This element was inserted by gergro on machine BBIPC0551.11-04-2018.11:19
        static void LæsEjendomme(string soapSecurity)
        {
            string webRequest = @"https://ws.fvst.dk:443/service/CHR_ejendomWS";
            string response =
                "/S:Envelope" +
                "/S:Body" +
                "/ns2:soegStamoplysningerMedAdresseInfoResponse" +
                "/ns2:CHREjendomSoegStamoplysningerMedAdresseInfoResponse" +
                "/ns2:Response";

            _info.Max = kommunekoder.Length;
            _info.Value = 0;
            _info.Spinner = false;

            foreach (var kommunekode in kommunekoder)
            {
                _info.Value++;
                _info.Report();

                string soapEnvelope = BuildSoapSøgStamoplysningerMedAdresseInfoEnvelope(GetSecurityHeader(_username, _password), kommunekode);

                XmlDocument doc = ReadWebService(webRequest, soapEnvelope);
                if (doc == null) return;

                XPathNavigator nav = doc.DocumentElement.CreateNavigator();
                XmlNamespaceManager msmgr = new XmlNamespaceManager(nav.NameTable);
                msmgr.AddNamespace("S", "http://schemas.xmlsoap.org/soap/envelope/");
                msmgr.AddNamespace("ns2", "http://www.logica.com/glrchr");
                XPathNodeIterator nodes = nav.Select(response, msmgr);
                if (nodes == null) return;

                while (nodes.MoveNext())
                {
                    var ChrNummer = Soap.GetXmlIntValue(nodes.Current, "ns2:ChrNummer", msmgr);
                    var Adresse = string.Intern(Soap.GetXmlStringValue(nodes.Current, "ns2:Ejendom/ns2:Adresse", msmgr));
                    var ByNavn = string.Intern(Soap.GetXmlStringValue(nodes.Current, "ns2:Ejendom/ns2:ByNavn", msmgr));
                    var PostNummer = Soap.GetXmlIntValue(nodes.Current, "ns2:Ejendom/ns2:PostNummer", msmgr);
                    var PostDistrikt = string.Intern(Soap.GetXmlStringValue(nodes.Current, "ns2:Ejendom/ns2:PostDistrikt", msmgr));
                    var KommuneNummer = Soap.GetXmlIntValue(nodes.Current, "ns2:Ejendom/ns2:KommuneNummer", msmgr);
                    var KommuneNavn = string.Intern(Soap.GetXmlStringValue(nodes.Current, "ns2:Ejendom/ns2:KommuneNavn", msmgr));
                    var StaldKoordinatX = Soap.GetXmlFloatValue(nodes.Current, "ns2:StaldKoordinatX", msmgr);
                    var StaldKoordinatY = Soap.GetXmlFloatValue(nodes.Current, "ns2:StaldKoordinatY", msmgr);

                    // Kommuner
                    if(!_kommuner.Any(item => item.KommuneNummer == KommuneNummer))
                    {
                        _kommuner.Add(new Kommune { KommuneNummer = KommuneNummer, KommuneNavn = KommuneNavn });
                    }

                    // Ejendomme
                    if(!_ejendomme.Any(item => item.CHR == ChrNummer))
                    {
                        _ejendomme.Add(new Ejendom
                        {
                            CHR = ChrNummer,
                            Adresse = Adresse,
                            ByNavn = ByNavn,
                            PostNummer = PostNummer,
                            PostDistrikt = PostDistrikt,
                            KommuneNummer = KommuneNummer,
                            KommuneNavn = KommuneNavn,
                            StaldKoordinatX = StaldKoordinatX,
                            StaldKoordinatY = StaldKoordinatY
                        });

                        _info.Ejendomme = _ejendomme.Count();

                    }
                }
            }

            LogWriter.GlobalInstance.WriteLine($"Ejendomme: {_ejendomme.Count}");
        }
        
        /// <summary>
        /// LÆSs the besætninger.
        /// </summary>
        /// <param name="soapSecurity">The SOAP security.</param>
        /// This element was inserted by gergro on machine BBIPC0551.11-04-2018.11:15
        static void LæsBesætninger(string soapSecurity)
        {
            #region Variable
            var webRequest = @"https://ws.fvst.dk:443/service/CHR_ejendomWS";
            var response =
                "/S:Envelope" +
                "/S:Body" +
                "/ns2:listBesaetningerResponse" +
                "/ns2:CHREjendomListBesaetningerResponse" +
                "/ns2:Response" +
                "/ns2:Besaetninger" +
                "/ns2:Besaetning";
            var retSvar = "/S:Envelope" +
                "/S:Body" +
                "/ns2:listBesaetningerResponse" +
                "/ns2:CHREjendomListBesaetningerResponse" +
                "/ns2:GLRCHRWSInfoOutbound";
            var index = 0;
            #endregion Variable

            _info.Spinner = true;
            _info.Max = _ejendomme.Count();
            _info.Value = 0;
            _info.BarColor = ConsoleColor.Magenta;
            _info.Spinner = false;

            foreach (var ejendom in _ejendomme)
            {
                _info.Value++;
                _info.Report();

                var soapEnvelope = BuildSoapListBesætningerEnvelope(GetSecurityHeader(_username, _password), ejendom.CHR.ToString());

                XmlDocument doc = ReadWebService(webRequest, soapEnvelope);
                if (doc == null) continue;
                var nav = doc.DocumentElement.CreateNavigator();
                var msmgr = new XmlNamespaceManager(nav.NameTable);
                msmgr.AddNamespace("S", "http://schemas.xmlsoap.org/soap/envelope/");
                msmgr.AddNamespace("ns2", "http://www.logica.com/glrchr");
                XPathNodeIterator nodes = nav.Select(retSvar, msmgr);
                if (nodes == null) continue;
                nodes.MoveNext();
                var returSvar = Soap.GetXmlStringValue(nodes.Current, "ns2:ReturSvar", msmgr);
                if (returSvar == "OK")
                {
                    if ((nodes = nav.Select(response, msmgr)) == null) continue;

                    while (nodes.MoveNext())
                    {
                        #region Variable
                        var BeseatningsID = 0;    // BESÆTNINGS ID
                        var EjerID = 0;
                        var BrugerID = 0;

                        // Besætning
                        var besætningsnummer = Soap.GetXmlLongValue(nodes.Current, "ns2:BesaetningsNummer", msmgr);
                        var dyreartskode = Soap.GetXmlIntValue(nodes.Current, "ns2:DyreArtKode", msmgr);
                        var dyreArtTekst = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:DyreArtTekst", msmgr));
                        var brugsArtKode = Soap.GetXmlIntValue(nodes.Current, "ns2:BrugsArtKode", msmgr);
                        var brugsArtTekst = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:BrugsArtTekst", msmgr));
                        var virksomhedsArtTekst = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:VirksomhedsArtTekst", msmgr));
                        var omsaetningsKode = Soap.GetXmlIntValue(nodes.Current, "ns2:OmsaetningsKode", msmgr);
                        var omsaetningsTekst = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:OmsaetningsTekst", msmgr));
                        // Ejer
                        var EjerCvrNummer = Soap.GetXmlIntValue(nodes.Current, @"ns2:Ejer/ns2:CvrNummer", msmgr);
                        var EjerNavn = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:Ejer/ns2:Navn", msmgr));
                        var EjerAdresse = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:Ejer/ns2:Adresse", msmgr));
                        var EjerPostNummer = Soap.GetXmlIntValue(nodes.Current, @"ns2:Ejer/ns2:PostNummer", msmgr);
                        var EjerByNavn = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:Ejer/ns2:ByNavn", msmgr));
                        var EjerPostDistrikt = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:Ejer/ns2:PostDistrikt", msmgr));
                        var EjerKommuneNummer = Soap.GetXmlIntValue(nodes.Current, @"ns2:Ejer/ns2:KommuneNummer", msmgr);
                        var EjerKommuneNavn = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:Ejer/ns2:KommuneNavn", msmgr));
                        var EjerReklamebeskyttelse = Soap.GetXmlBoolValue(nodes.Current, @"ns2:Ejer/ns2:Reklamebeskyttelse", msmgr);
                        // Bruger
                        var BrugerCvrNummer = Soap.GetXmlIntValue(nodes.Current, @"ns2:Bruger/ns2:CvrNummer", msmgr);
                        var BrugerNavn = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:Bruger/ns2:Navn", msmgr));
                        var BrugerAdresse = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:Bruger/ns2:Adresse", msmgr));
                        var BrugerPostNummer = Soap.GetXmlIntValue(nodes.Current, @"ns2:Bruger/ns2:PostNummer", msmgr);
                        var BrugerByNavn = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:Bruger/ns2:ByNavn", msmgr));
                        var BrugerPostDistrikt = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:Bruger/ns2:PostDistrikt", msmgr));
                        var BrugerKommuneNummer = Soap.GetXmlIntValue(nodes.Current, @"ns2:Bruger/ns2:KommuneNummer", msmgr);
                        var BrugerKommuneNavn = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:Bruger/ns2:KommuneNavn", msmgr));
                        var BrugerReklamebeskyttelse = Soap.GetXmlBoolValue(nodes.Current, @"ns2:Bruger/ns2:Reklamebeskyttelse", msmgr);
                        // Praksis
                        var PraksisNr = Soap.GetXmlIntValue(nodes.Current, @"ns2:BesPraksis/ns2:PraksisNr", msmgr);
                        var PraksisNavn = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:BesPraksis/ns2:PraksisNavn", msmgr));
                        var PraksisAdresse = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:BesPraksis/ns2:PraksisAdresse", msmgr));
                        var PraksisPostNummer = Soap.GetXmlIntValue(nodes.Current, @"ns2:BesPraksis/ns2:PraksisPostNummer", msmgr);
                        var PraksisByNavn = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:BesPraksis/ns2:PraksisByNavn", msmgr));
                        var PraksisPostDistrikt = string.Intern(Soap.GetXmlStringValue(nodes.Current, @"ns2:BesPraksis/ns2:PraksisPostDistrikt", msmgr));
                        #endregion Variable

                        // Kommuner
                        {
                            if (!_kommuner.Any(item => item.KommuneNummer == EjerKommuneNummer))
                            {
                                _kommuner.Add(new Kommune { KommuneNummer = EjerKommuneNummer, KommuneNavn = EjerKommuneNavn });
                                _info.Kommuner = _kommuner.Count;
                            }
                            if (!_kommuner.Any(item => item.KommuneNummer == BrugerKommuneNummer))
                            {
                                _kommuner.Add(new Kommune { KommuneNummer = BrugerKommuneNummer, KommuneNavn = BrugerKommuneNavn });
                                _info.Kommuner = _kommuner.Count;
                            }
                        }
                        
                        // Ejere
                        {
                            Ejer ejer = EjerCvrNummer == 0 
                                ? _ejere.Find(e => e.CVR == EjerCvrNummer && e.Navn == EjerNavn && e.Adresse == EjerAdresse && e.PostNummer == EjerPostNummer && e.PostDistrikt == EjerPostDistrikt) 
                                : _ejere.Find(e => e.CVR == EjerCvrNummer);
                            EjerID = ejer == null ? _ejere.Count + 1 : ejer.EjerID;
                            if (ejer == null)
                            {
                                _ejere.Add(new Ejer
                                {
                                    EjerID = EjerID,
                                    CVR = EjerCvrNummer,
                                    Navn = EjerNavn,
                                    Adresse = EjerAdresse,
                                    PostNummer = EjerPostNummer,
                                    PostDistrikt = EjerPostDistrikt,
                                    KommuneNummer = EjerKommuneNummer,
                                    KommuneNavn = EjerKommuneNavn,
                                    ByNavn = EjerByNavn,
                                    Reklamebeskyttelse = EjerReklamebeskyttelse
                                });
                                _info.Ejere = _ejere.Count();
                            }
                            else
                            {
                                EjerID = ejer.EjerID;
                            }
                        }

                        // EjendomListEjer
                        {
                            if(!_ejendomlistejer.Any(s => s.CHR == ejendom.CHR && s.EjerID == EjerID))
                            {
                                _ejendomlistejer.Add(new EjendomListEjer
                                {
                                    CHR = ejendom.CHR,
                                    EjerID = EjerID
                                });
                                _info.EjendomListEjer = _ejendomlistejer.Count;
                            }
                        }

                        // DyreArt
                        {
                            if(!_dyreart.Any(d => d.DyreArtKode == dyreartskode))
                            {
                                _dyreart.Add(new DyreArt { DyreArtKode = dyreartskode, DyreArtTekst = dyreArtTekst });
                                _info.Dyreart = _dyreart.Count();
                            }
                        }

                        // BrugsArt
                        {
                            if(!_brugsarter.Any(b => b.BrugsArtKode == brugsArtKode))
                            {
                                _brugsarter.Add(new BrugsArt { BrugsArtKode = brugsArtKode, BrugsArtTekst = brugsArtTekst });
                                _info.Brugasrter = _brugsarter.Count();
                            }
                        }

                        // Omsætning
                        {
                            if(!_omsaetning.Any(b => b.OmsaetningsKode == omsaetningsKode))
                            {
                                _omsaetning.Add(new Omsaetning
                                {
                                    OmsaetningsKode = omsaetningsKode,
                                    OmsaetningsTekst = omsaetningsTekst
                                });
                                _info.Omsætning = _omsaetning.Count;
                            }
                        }

                        // Besætning
                        {
                            Besætning besætning = _besætninger.Find(b => b.besaetningsNummer == besætningsnummer);
                            BeseatningsID = besætning == null ? _besætninger.Count + 1 : besætning.BeseatningsID;
                            if (besætning == null)
                            {
                                _besætninger.Add(new Besætning
                                {
                                    BeseatningsID = BeseatningsID,
                                    besaetningsNummer = besætningsnummer,
                                    dyreArtKode = dyreartskode,
                                    brugsArtKode = brugsArtKode,
                                    virksomhedsArtTekst = virksomhedsArtTekst,
                                    omsaetningsKode = omsaetningsKode,
                                });
                                _info.Besætninger = _besætninger.Count();
                            }
                        }

                        // EjendomListBesaetning
                        {
                            if(!_ejendomlistbesaetning.Any(s => s.CHR == ejendom.CHR && s.BeseatningsID == BeseatningsID && s.BesaetningsNummer == besætningsnummer))
                            {
                                _ejendomlistbesaetning.Add(new EjendomListBesaetning
                                {
                                    BesaetningsNummer = besætningsnummer,
                                    BeseatningsID = BeseatningsID,
                                    CHR = ejendom.CHR
                                });
                                _info.EjendomListBesaetning = _ejendomlistbesaetning.Count;
                            }
                        }

                        // BesætningsStørrelseType
                        {
                            XPathNodeIterator cnodes = nodes.Current.Select("ns2:BesStr", msmgr);
                            while (cnodes.MoveNext())
                            {
                                var BesaetningsStoerrelseTekst = Soap.GetXmlStringValue(cnodes.Current, @"ns2:BesaetningsStoerrelseTekst", msmgr);
                                var BesaetningsStoerrelse = Soap.GetXmlIntValue(cnodes.Current, "ns2:BesaetningsStoerrelse", msmgr);

                                if(!_besætningstyper.Any(b => b.BesaetningsNummer == besætningsnummer && b.BeseatningsID == BeseatningsID && b.EjerID == EjerID && b.BesaetningsStoerrelseTekst == BesaetningsStoerrelseTekst && b.DyreArtKode == dyreartskode))
                                {
                                    _besætningstyper.Add(new BesætningsType {
                                        BeseatningsID = BeseatningsID,
                                        EjerID = EjerID,
                                        BesaetningsNummer = besætningsnummer,
                                        DyreArtKode = dyreartskode,
                                        BesaetningsStoerrelseTekst = BesaetningsStoerrelseTekst,
                                        BesaetningsStoerrelse = BesaetningsStoerrelse
                                    });
                                    _info.BesætningsTyper = _besætningstyper.Count();
                                }
                            }
                        }

                        // Bruger
                        {
                            Bruger bruger = BrugerCvrNummer == 0 
                                ? _brugere.Find(e => e.CVR == BrugerCvrNummer && e.Navn == BrugerNavn && e.Adresse == BrugerAdresse && e.PostNummer == BrugerPostNummer && e.PostDistrikt == BrugerPostDistrikt) 
                                : _brugere.Find(e => e.CVR == BrugerCvrNummer);
                            if (bruger == null)
                            {
                                BrugerID = _brugere.Count + 1;
                                _brugere.Add(new Bruger
                                {
                                    BrugerID = BrugerID,
                                    CVR = BrugerCvrNummer,
                                    Navn = BrugerNavn,
                                    Adresse = BrugerAdresse,
                                    PostNummer = BrugerPostNummer,
                                    PostDistrikt = BrugerPostDistrikt,
                                    KommuneNummer = BrugerKommuneNummer,
                                    KommuneNavn = BrugerKommuneNavn,
                                    ByNavn = BrugerByNavn,
                                    Reklamebeskyttelse = BrugerReklamebeskyttelse
                                });
                                _info.Brugere = _brugere.Count();
                            }
                            else
                            {
                                BrugerID = bruger.BrugerID;
                            }
                        }

                        // EjendomListBruger
                        {
                            if(!_ejendomlistbruger.Any(s => s.CHR == ejendom.CHR && s.BrugerID == BrugerID))
                            {
                                _ejendomlistbruger.Add(new EjendomListBruger
                                {
                                    BrugerID = BrugerID,
                                    CHR = ejendom.CHR
                                });
                                _info.EjendomListBruger = _ejendomlistbruger.Count;
                            }
                        }

                        // Praksis
                        {
                            if (!_praksis.Any(p => p.PraksisNr == PraksisNr))
                            {
                                _praksis.Add(new Praksis {
                                    PraksisNr = PraksisNr,
                                    Navn = PraksisNavn,
                                    Adresse = PraksisAdresse,
                                    PostNummer = PraksisPostNummer,
                                    ByNavn = PraksisByNavn,
                                    PostDistrikt = PraksisPostDistrikt
                                });
                                _info.Praksis = _praksis.Count();
                            }
                        }

                        // EjendomListDyrlaege
                        {
                            if(!_ejendomlistdyrlaege.Any(s => s.CHR == ejendom.CHR && s.PraksisNr == PraksisNr))
                            {
                                _ejendomlistdyrlaege.Add(new EjendomListDyrlaege
                                {
                                    CHR = ejendom.CHR,
                                    PraksisNr = PraksisNr
                                });
                                _info.EjendomListDyrlaege = _ejendomlistdyrlaege.Count;
                            }
                        }

                    }
                }
            }

            LogWriter.GlobalInstance.WriteLine($"Ejere: {_ejere.Count}");
            LogWriter.GlobalInstance.WriteLine($"EjendomListEjer: {_ejendomlistejer.Count}");
            LogWriter.GlobalInstance.WriteLine($"DyreArt: {_dyreart.Count}");
            LogWriter.GlobalInstance.WriteLine($"Omsaetning: {_omsaetning.Count}");
            LogWriter.GlobalInstance.WriteLine($"Besætninger: {_besætninger.Count}");
            LogWriter.GlobalInstance.WriteLine($"EjendomListBesaetning: {_ejendomlistbesaetning.Count}");
            LogWriter.GlobalInstance.WriteLine($"BesætningsTyper: {_besætningstyper.Count}");
            LogWriter.GlobalInstance.WriteLine($"Brugere: {_brugere.Count}");
            LogWriter.GlobalInstance.WriteLine($"EjendomListBruger: {_ejendomlistbruger.Count}");
            LogWriter.GlobalInstance.WriteLine($"Praksis: {_praksis.Count}");
            LogWriter.GlobalInstance.WriteLine($"EjendomListDyrlaege: {_ejendomlistdyrlaege.Count}");

        }

        /// <summary>
        /// LÆSs the veterinære haendelser.
        /// </summary>
        /// <param name="soapSecurity">The SOAP security.</param>
        /// This element was inserted by gergro on machine BBIPC0551.09-08-2018.09:48
        static void LæsVeterinæreHaendelser(string soapSecurity)
        {
            _info.Spinner = true;
            _info.Max = _ejendomme.Count();
            _info.Value = 0;
            _info.BarColor = ConsoleColor.Green;
            _info.Spinner = false;

            string webRequest = @"https://ws.fvst.dk:443/service/CHR_ejendomWS";
            string response =
                "/S:Envelope" +
                "/S:Body" +
                "/ns2:hentVeterinaereHaendelserResponse" +
                "/ns2:CHREjendomHentVeterinaereHaendelserResponse" +
                "/ns2:Response" +
                "/ns2:VeterinaereHaendelser";


            foreach (var ejendom in _ejendomme)
            {
                _info.Value++;
                _info.Report();


                string soapEnvelope = BuildSoapHentVeterinaereHaendelserEnvelope(GetSecurityHeader(_username, _password), ejendom.CHR.ToString());

                XmlDocument doc = ReadWebService(webRequest, soapEnvelope);
                if (doc == null) continue;

                var nav = doc.DocumentElement.CreateNavigator();
                var msmgr = new XmlNamespaceManager(nav.NameTable);
                msmgr.AddNamespace("S", "http://schemas.xmlsoap.org/soap/envelope/");
                msmgr.AddNamespace("ns2", "http://www.logica.com/glrchr");
                XPathNodeIterator nodes = nav.Select(response, msmgr);
                if (nodes == null) continue;

                while (nodes.MoveNext())
                {
                    bool VeterinaereProblemer = Soap.GetXmlBoolValue(nodes.Current, "ns2:VeterinaereProblemer", msmgr);                    
                    if(!_veterinaerehaendelser.Any(s => s.CHR == ejendom.CHR))
                    {
                        _veterinaerehaendelser.Add(new VeterinaereHaendelser
                        {
                            CHR = ejendom.CHR,
                            VeterinaereProblemer = VeterinaereProblemer
                        });
                        _info.VeterinaereHaendelser = _veterinaerehaendelser.Count;
                    }

                    XPathNodeIterator cnodes = nodes.Current.Select("ns2:VeterinaerHaendelse", msmgr);
                    if (cnodes == null) continue;

                    while (cnodes.MoveNext())
                    {
                        int DyreArtKode = Soap.GetXmlIntValue(cnodes.Current, "ns2:DyreArtKode", msmgr);
                        string DyreArtTekst = Soap.GetXmlStringValue(cnodes.Current, "ns2:DyreArtTekst", msmgr);
                        string SygdomsKode = Soap.GetXmlStringValue(cnodes.Current, "ns2:SygdomsKode", msmgr);
                        string SygdomsTekst = Soap.GetXmlStringValue(cnodes.Current, "ns2:SygdomsTekst", msmgr);
                        string VeterinaerStatusKode = Soap.GetXmlStringValue(cnodes.Current, "ns2:VeterinaerStatusKode", msmgr);
                        string VeterinaerStatusTekst = Soap.GetXmlStringValue(cnodes.Current, "ns2:VeterinaerStatusTekst", msmgr);
                        int SygdomsNiveauKode = Soap.GetXmlIntValue(cnodes.Current, "ns2:SygdomsNiveauKode", msmgr);
                        string SygdomsNiveauTekst = Soap.GetXmlStringValue(cnodes.Current, "ns2:SygdomsNiveauTekst", msmgr);
                        DateTime DatoVeterinaerStatus = Soap.GetXmlDateTimeValue(cnodes.Current, "ns2:DatoVeterinaerStatus", msmgr);

                        // DyreArt
                        if(!_dyreart.Any(s => s.DyreArtKode == DyreArtKode))
                        {
                            _dyreart.Add(new DyreArt
                            {
                                DyreArtKode = DyreArtKode,
                                DyreArtTekst = DyreArtTekst
                            });
                            _info.Dyreart = _dyreart.Count;
                        }

                        // Sygdom
                        if(!_sygdom.Any(s => s.SygdomsKode == SygdomsKode))
                        {
                            _sygdom.Add(new Sygdom
                            {
                                SygdomsKode = SygdomsKode,
                                SygdomsTekst = SygdomsTekst
                            });
                            _info.Sygdom = _sygdom.Count;
                        }

                        // VeterinaerStatus
                        if(!_veterinaerstatus.Any(s => s.VeterinaerStatusKode == VeterinaerStatusKode))
                        {
                            _veterinaerstatus.Add(new VeterinaerStatus
                            {
                                VeterinaerStatusKode = VeterinaerStatusKode,
                                VeterinaerStatusTekst = VeterinaerStatusTekst
                            });
                            _info.VeterinaerStatus = _veterinaerstatus.Count;
                        }

                        // SygdomsNiveau
                        if(!_sygdomsniveau.Any(s => s.SygdomsNiveauKode == SygdomsNiveauKode))
                        {
                            _sygdomsniveau.Add(new SygdomsNiveau
                            {
                                SygdomsNiveauKode = SygdomsNiveauKode,
                                SygdomsNiveauTekst = SygdomsNiveauTekst
                            });
                            _info.SygdomsNiveau = _sygdomsniveau.Count;
                        }

                        // VeterinaereHaendelse
                        if(!_veterinaerehaendelse.Any(s => s.CHR == ejendom.CHR && s.DyreArtKode == DyreArtKode && s.SygdomsKode == SygdomsKode && s.VeterinaerStatusKode == VeterinaerStatusKode && s.SygdomsNiveauKode == SygdomsNiveauKode))
                        {
                            _veterinaerehaendelse.Add(new VeterinaereHaendelse
                            {
                                ID = _veterinaerehaendelse.Count + 1,
                                CHR = ejendom.CHR,
                                DyreArtKode = DyreArtKode,
                                SygdomsKode = SygdomsKode,
                                SygdomsNiveauKode = SygdomsNiveauKode,
                                VeterinaerStatusKode = VeterinaerStatusKode,
                                DatoVeterinaerStatus = DatoVeterinaerStatus
                            });
                            _info.VeterinaereHaendelse = _veterinaerehaendelse.Count;
                        }
                    }
                }

            }

            LogWriter.GlobalInstance.WriteLine($"VeterinaereHaendelser: {_veterinaerehaendelser.Count}");
            LogWriter.GlobalInstance.WriteLine($"DyreArt: {_dyreart.Count}");
            LogWriter.GlobalInstance.WriteLine($"Sygdom: {_sygdom.Count}");
            LogWriter.GlobalInstance.WriteLine($"VeterinaerStatus: {_veterinaerstatus.Count}");
            LogWriter.GlobalInstance.WriteLine($"SygdomsNiveau: {_sygdomsniveau.Count}");
            LogWriter.GlobalInstance.WriteLine($"VeterinaereHaendelse: {_veterinaerehaendelse.Count}");
        }

        /// <summary>
        /// Reads the web service.
        /// </summary>
        /// <param name="webrequest">The webrequest.</param>
        /// <param name="soapenvelope">The soapenvelope.</param>
        /// <param name="soapsecurity">The soapsecurity.</param>
        /// <returns></returns>
        /// This element was inserted by gergro on machine BBIPC0551.11-04-2018.11:15
        /// <exception cref="InvalidOperationException"></exception>
        /// <exception cref="Exception">Max tries</exception>
        public static XmlDocument ReadWebService(string webrequest, string soapenvelope)
        {
            int MaxTry = _maxtry;
            int SleepOneHour = 1000 * 60 * 10;
            XmlDocument doc = null;


            while (MaxTry > 0)
            {
                HttpWebRequest request = HttpWebRequest.Create(webrequest) as HttpWebRequest;
                request.ContentType = "text/xml;charset=\"utf-8\"";
                request.Accept = "text/xml";
                request.Method = "POST";
                request.ContentLength = soapenvelope.Length;
                doc = null;
                StreamReader reader = null;
                WebResponse response = null;

                try
                {
                    Stream requestStream = request.GetRequestStream();
                    using (StreamWriter streamWriter = new StreamWriter(requestStream))
                    {
                        streamWriter.Write(soapenvelope);
                    }

                    response = request.GetResponse();
                    Stream responseStream = response.GetResponseStream();

                    reader = new StreamReader(responseStream ?? throw new InvalidOperationException());
                    string result = reader.ReadToEnd();

                    doc = new XmlDocument();
                    doc.LoadXml(result);
                    doc.PreserveWhitespace = true;
                    MaxTry = 0;
                }
                catch (Exception ex)
                {
                    _info.Stopwatch1.Stop();
                    _info.Stopwatch2.Start();
                    _info.Retry++;
                    _info.Report();
                    MaxTry--;

                    _info.LatestError = ex.Message.Replace("\r\n", " ");

                    if (MaxTry == 0)
                    {
                        Thread.Sleep(SleepOneHour);
                        MaxTry = _maxtry;
                    }
                    else
                    {
                        Thread.Sleep(_sleeptime);
                        MaxTry = _maxtry;
                    }
                    _info.Stopwatch2.Stop();
                    _info.Stopwatch1.Start();
                }
                finally
                {
                    reader?.Close();
                    response?.Close();
                }
            }
            return doc;
        }

        /// <summary>
        /// Gets the security header.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        /// This element was inserted by gergro on machine BBIPC0551.11-04-2018.12:13
        public static string GetSecurityHeader(string username, string password)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(password);
            password = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            
            Random r = new Random();
            DateTime created = DateTime.Now;
            var _createdStr = created.ToString("yyyy-MM-ddThh:mm:ss.fffZ");
            var nonce = Convert.ToBase64String(Encoding.ASCII.GetBytes(Soap.SHA1Encrypt(created + r.Next().ToString())));
            
            return
                $"<soapenv:Header>" +
                $"   <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" +
                $"      <wsse:UsernameToken wsu:Id=\"UsernameToken-4839C12F61E48ABE9D14405885321423\">" +
                $"         <wsse:Username>{username}</wsse:Username>" +
                $"         <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">{password}</wsse:Password>" +
                $"         <wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">{nonce}</wsse:Nonce>" +
                $"         <wsu:Created>{created}</wsu:Created>" +
                $"      </wsse:UsernameToken>" +
                $"   </wsse:Security>" +
                $"</soapenv:Header>";
        }

        /// <summary>
        /// Builds the SOAP SØG stamoplysninger med adresse information envelope.
        /// </summary>
        /// <param name="soapSecurity">The SOAP security.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// This element was inserted by gergro on machine BBIPC0551.09-08-2018.09:48
        public static string BuildSoapSøgStamoplysningerMedAdresseInfoEnvelope(string soapSecurity, string value)
        {
            return 
            $"<soapenv:Envelope xmlns:glr=\"http://www.logica.com/glrchr\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
            $"{soapSecurity}" +
            $"   <soapenv:Body>" +
            $"      <glr:soegStamoplysningerMedAdresseInfo>" +
            $"         <CHREjendomSoegStamoplysningerMedAdresseInfoRequest>" +
            $"            <glr:GLRCHRWSInfoInbound>" +
            $"               <glr:KlientId>?</glr:KlientId>" +
            $"               <glr:BrugerNavn>?</glr:BrugerNavn>" +
            $"               <glr:SessionId>?</glr:SessionId>" +
            $"               <glr:IPAdresse>?</glr:IPAdresse>" +
            $"               <glr:TrackID>?</glr:TrackID>" +
            $"            </glr:GLRCHRWSInfoInbound>" +
            $"            <glr:Request>" +
            $"               <glr:Ejendom>" +
            $"                  <glr:KommuneNummer>{value}</glr:KommuneNummer>" +
            $"               </glr:Ejendom>" +
            $"            </glr:Request>" +
            $"         </CHREjendomSoegStamoplysningerMedAdresseInfoRequest>" +
            $"      </glr:soegStamoplysningerMedAdresseInfo>" +
            $"   </soapenv:Body>" +
            $"</soapenv:Envelope>";

        }

        /// <summary>
        /// Builds the SOAP list besætninger envelope.
        /// </summary>
        /// <param name="soapSecurity">The SOAP security.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// This element was inserted by gergro on machine BBIPC0551.09-08-2018.09:48
        public static string BuildSoapListBesætningerEnvelope(string soapSecurity, string value)
        {
            return
                $"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:glr=\"http://www.logica.com/glrchr\">" +
                $"{soapSecurity}" +
                $"   <soapenv:Body>" +
                $"      <glr:listBesaetninger>" +
                $"         <CHR_ejendomListBesaetningerRequest>" +
                $"            <glr:GLRCHRWSInfoInbound>" +
                $"               <glr:KlientId>?</glr:KlientId>" +
                $"               <glr:BrugerNavn>?</glr:BrugerNavn>" +
                $"               <glr:SessionId>?</glr:SessionId>" +
                $"               <glr:IPAdresse>?</glr:IPAdresse>" +
                $"               <glr:TrackID>?</glr:TrackID>" +
                $"            </glr:GLRCHRWSInfoInbound>" +
                $"            <glr:Request>" +
                $"               <glr:ChrNummer>{value}</glr:ChrNummer>" +
                $"            </glr:Request>" +
                $"         </CHR_ejendomListBesaetningerRequest>" +
                $"      </glr:listBesaetninger>" +
                $"   </soapenv:Body>" +
                $"</soapenv:Envelope>";
        }

        /// <summary>
        /// Builds the SOAP hent veterinaere haendelser envelope.
        /// </summary>
        /// <param name="soapSecurity">The SOAP security.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// This element was inserted by gergro on machine BBIPC0551.09-08-2018.09:48
        public static string BuildSoapHentVeterinaereHaendelserEnvelope(string soapSecurity, string value)
        {
            return
                $"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:glr=\"http://www.logica.com/glrchr\">" +
                $"{soapSecurity}" +
                $"   <soapenv:Body>" +
                $"      <glr:hentVeterinaereHaendelser>" +
                $"         <CHR_ejendomHentVeterinaereHaendelserRequest>" +
                $"            <glr:GLRCHRWSInfoInbound>" +
                $"               <glr:KlientId>?</glr:KlientId>" +
                $"               <glr:BrugerNavn>?</glr:BrugerNavn>" +
                $"               <glr:SessionId>?</glr:SessionId>" +
                $"               <glr:IPAdresse>?</glr:IPAdresse>" +
                $"               <glr:TrackID>?</glr:TrackID>" +
                $"            </glr:GLRCHRWSInfoInbound>" +
                $"            <glr:Request>" +
                $"               <glr:ChrNummer>{value}</glr:ChrNummer>" +
                $"            </glr:Request>" +
                $"         </CHR_ejendomHentVeterinaereHaendelserRequest>" +
                $"      </glr:hentVeterinaereHaendelser>" +
                $"   </soapenv:Body>" +
                $"</soapenv:Envelope>";
        }

        /// <summary>
        /// Checks the required fields.
        /// </summary>
        /// <returns></returns>
        /// This element was inserted by gergro on machine BBIPC0551.09-08-2018.08:41
        private static bool checkRequiredFields()
        {


            return true;
        }

    }

}
