﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CGI_CHR_loader
{
    public static class Phonetic
    {
        /// <!-- =================================================== -->
        /// <summary>
        ///     Match 2 string 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="cmp"></param>
        /// <param name="exactmatch"></param>
        /// <returns></returns>
        /// <!-- =================================================== -->
        public static bool Match(string str, string cmp, int exactmatch = 0)
        {
            List<string> lstFind = new List<string>();
            List<string> lstName = new List<string>();

            SplitNumberFromAlphanumeric(str, lstFind);
            SplitNumberFromAlphanumeric(cmp, lstName);


            bool bRet = false;
            //char[] delim = { ' ', ',', '.', '\\', '/', '|', '\t', '\'', '\"' };
            //string[] txtFind = str.ToLower().Trim().Split(delim, StringSplitOptions.RemoveEmptyEntries);
            //string[] names   = cmp.ToLower().Trim().Split(delim, StringSplitOptions.RemoveEmptyEntries);

            int m = 0;
            foreach (string find in lstFind)
            {
                if (exactmatch == 0)
                {
                    bRet = lstName.Contains(find);

                    if (!bRet)
                        break;
                }
                else if (exactmatch == 100)
                {
                    List<string> matches = new List<string>();
                    string code = MetaphoneSearch(find, lstName, matches);
                    bRet = (matches.Count > 0);

                    if (!bRet)
                        break;
                }
                else
                {
                    List<string> matches = new List<string>();
                    string code = MetaphoneSearch(find, lstName, matches);
                    m += (matches.Count > 0) ? 1 : 0;

                }
            }
            if (m > 0)
                bRet = ((100L / lstFind.Count) * m >= exactmatch);

            return bRet;
        }

        private static string SoundexSearch(string find, string[] names, List<string> matches)
        {
            // Encode string we want to find
            string code = Soundex.Encode(find);

            // Search through the list of names
            foreach (string name in names)
            {
                // Compare against soundex-encoded version of name
                if (Soundex.Encode(name) == code)
                {
                    // Found a match--add it to list
                    matches.Add(name);
                }
            }
            return code;
        }

        /// <!-- =================================================== -->
        /// <summary>
        /// 
        /// </summary>
        /// <param name="find"></param>
        /// <param name="names"></param>
        /// <param name="matches"></param>
        /// <returns></returns>
        /// <!-- =================================================== -->
        private static string MetaphoneSearch(string find, List<string> names, List<string> matches)
        {
            // Encode string we want to find
            Metaphone metaphone = new Metaphone();
            string code = metaphone.Encode(find);

            // Search through the list of names
            foreach (string name in names)
            {
                // Compare against soundex-encoded version of name
                if (metaphone.Encode(name) == code)
                {
                    // Found a match--add it to list
                    matches.Add(name);
                }
            }
            return code;
        }

        /// <!-- =================================================== -->
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="lst"></param>
        /// <!-- =================================================== -->
        public static void SplitNumberFromAlphanumeric(string str, List<string> lst)
        {
            char[] delim = { ' ', ',', '.', '\\', '/', '|', '\t', '\'', '\"' };
            string[] items = str.ToLower().Trim().Split(delim, StringSplitOptions.RemoveEmptyEntries);
            foreach (string item in items)
            {
                string[] res = Regex.Split(item, "(?<Numeric>[0-9]*)(?<Alpha>[a-zA-Z]*)");
                foreach (string resitem in res)
                {
                    if (!string.IsNullOrEmpty(resitem.Trim()))
                        lst.Add(resitem);
                }
            }
        }

    }
}
