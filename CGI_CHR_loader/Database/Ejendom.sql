IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Ejendom]') AND type in (N'U'))
DROP TABLE [dbo].[Ejendom]
GO

CREATE TABLE [dbo].[Ejendom](
	[CHR] [int] NOT NULL,
	[Adresse] [nvarchar](128) NULL,
	[Postnummer] [int] NULL,
	[ByNavn] [nvarchar](128) NULL,
	[PostDistrikt] [nvarchar](128) NULL,
	[KommuneID] [int] NULL,
	[StaldKoordinatX] [real] NULL,
	[StaldKoordinatY] [real] NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
 CONSTRAINT [PKN_Ejendom] PRIMARY KEY CLUSTERED 
(
	[CHR] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
