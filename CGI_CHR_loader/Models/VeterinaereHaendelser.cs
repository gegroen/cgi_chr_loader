﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    class VeterinaereHaendelser
    {
        public int CHR { get; set; }
        public bool VeterinaereProblemer { get; set; }
    }
}
