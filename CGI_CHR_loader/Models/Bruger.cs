﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CGI_CHR_loader.Models
{
    public class Bruger
    {
        public int BrugerID { get; set; }
        public int CVR { get; set; }
        public string Navn { get; set; }
        public string Adresse { get; set; }
        public int PostNummer { get; set; }
        public string ByNavn { get; set; }
        public string PostDistrikt { get; set; }
        public int KommuneNummer { get; set; }
        public string KommuneNavn { get; set; }
        public bool Reklamebeskyttelse { get; set; }
    }
}
